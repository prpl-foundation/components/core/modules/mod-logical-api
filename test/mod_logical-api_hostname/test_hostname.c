/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>

#include "test_hostname.h"
#include "common_utils.h"

int test_setup(void** state) {
    unittests_setup_t ctx;
    ctx.include_dns = true;
    mod_logical_unittests_setup(state, &ctx);
    return 0;
}

int test_setup_dont_include_dns_odl(void** state) {
    unittests_setup_t ctx;
    ctx.include_dns = false;
    mod_logical_unittests_setup(state, &ctx);
    return 0;
}

int test_teardown(void** state) {
    mod_logical_unittests_teardown(state);
    return 0;
}

void test_hostname(UNUSED void** state) {
    amxd_object_t* hostname_multi_inst_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.HostName");
    amxd_object_t* hostname_obj = NULL;
    amxd_trans_t* trans = NULL;

    amxd_trans_new(&trans);

    assert_non_null(hostname_multi_inst_obj);

    expect_check_set_host("myhost", "Device.Logical.Interface.2.", "prplOS");
    amxd_trans_select_object(trans, hostname_multi_inst_obj);
    amxd_trans_add_inst(trans, 0, "myhost");
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    hostname_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.HostName.myhost.");
    assert_non_null(hostname_obj);

    expect_check_remove_host("myhost");
    amxd_trans_select_object(trans, hostname_obj);
    amxd_trans_set_value(bool, trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    amxd_trans_select_object(trans, hostname_obj);
    amxd_trans_set_value(cstring_t, trans, "Name", "newhostname");
    amxd_trans_set_value(uint32_t, trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    expect_check_set_host("myhost", "Device.Logical.Interface.2.", "newhostname");
    amxd_trans_select_object(trans, hostname_obj);
    amxd_trans_set_value(bool, trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    expect_check_set_host_name("myhost", "newhostname2");
    amxd_trans_select_object(trans, hostname_obj);
    amxd_trans_set_value(cstring_t, trans, "Name", "newhostname2");
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    expect_check_set_host("myhost", "Device.Logical.Interface.2.", "newhostname2");
    amxd_trans_select_object(trans, hostname_obj);
    amxd_trans_set_value(uint32_t, trans, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    expect_check_remove_host("myhost");
    amxd_trans_select_object(trans, hostname_multi_inst_obj);
    amxd_trans_del_inst(trans, 0, "myhost");
    assert_int_equal(amxd_trans_apply(trans, amxut_bus_dm()), 0);
    amxd_trans_clean(trans);
    amxut_bus_handle_events();

    amxd_trans_delete(&trans);
}

void test_hostname_wait_for_dns(UNUSED void** state) {
    include_dns();
    amxut_bus_handle_events();

    amxp_sigmngr_emit_signal(NULL, "wait:DNS.", NULL); // usually amxrt would do this
    expect_check_set_host("hostname-1", "Device.Logical.Interface.2.", "prplOS");
    amxut_bus_handle_events();
}