/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>

#include "test_lan_api.h"
#include "logical_api_getters.h"
#include "logical_api_utils.h"
#include "common_utils.h"

int test_setup(void** state) {
    unittests_setup_t ctx;
    ctx.include_dns = true;
    mod_logical_unittests_setup(state, &ctx);
    amxp_sigmngr_emit_signal(NULL, "wait:done", NULL);
    return 0;
}

int test_teardown(void** state) {
    mod_logical_unittests_teardown(state);
    return 0;
}

void test_private_ipv4_config(UNUSED void** state) {
    amxd_object_t* lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.");
    amxd_object_t* config_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Subnet.Config.");
    amxc_var_t ret;
    amxc_var_t parameters;

    assert_non_null(lan_obj);
    assert_non_null(config_obj);

    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);
    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetIPv4Config", &parameters, &ret), 0);
    assert_true(GET_BOOL(&ret, "Enable"));
    assert_string_equal("192.168.1.1", GET_CHAR(&ret, "IPAddress"));
    assert_string_equal("255.255.255.0", GET_CHAR(&ret, "SubnetMask"));
    assert_true(GETP_BOOL(&ret, "DHCPParameters.Enable"));
    assert_int_equal(43200, GETP_UINT32(&ret, "DHCPParameters.LeaseTime"));
    assert_string_equal("192.168.1.249", GETP_CHAR(&ret, "DHCPParameters.MaxAddress"));
    assert_string_equal("192.168.1.100", GETP_CHAR(&ret, "DHCPParameters.MinAddress"));
    assert_string_equal("lan", GETP_CHAR(&ret, "DHCPParameters.Pool"));
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.168.2.1");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    // Fails because IPAddress conflicts with guest interface
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_invalid_function_argument);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.169.12.11");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    // Fails because IPAddress is not in valid subnet set in mock_subnet.odl
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_invalid_function_argument);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.168.8.1");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    // Succeeds because IPAddress is in valid subnet set in mock_subnet.odl
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.168.8.1");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    amxd_object_set_value(cstring_t, config_obj, "InvalidSubnets", "192.168.8.0/24");
    amxd_object_set_value(cstring_t, config_obj, "ValidSubnets", "");
    // Fails because IPAddress is in invalid subnet
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_invalid_function_argument);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.167.8.1");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    amxd_object_set_value(cstring_t, config_obj, "InvalidSubnets", "192.168.8.0/16");
    amxd_object_set_value(cstring_t, config_obj, "ValidSubnets", "");
    // Succeeds because IPAddress is not in invalid subnet
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "91.176.0.50");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    amxd_object_set_value(cstring_t, config_obj, "ValidSubnets", "91.176.140.205/15");
    // Succeeds because IPAddress is in valid subnet
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "91.177.250.253");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(bool, &parameters, "DHCPEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "MinAddress", "192.168.8.100");
    amxc_var_add_key(cstring_t, &parameters, "MaxAddress", "192.168.8.249");
    amxc_var_add_key(cstring_t, &parameters, "Pool", "newpool");
    amxc_var_add_key(cstring_t, &parameters, "LeaseTime", "40000");
    // Succeeds because IPAddress is in valid subnet
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "192.168.8.12");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.255.0");
    amxd_object_set_value(cstring_t, config_obj, "ValidSubnets", "");
    amxd_object_set_value(cstring_t, config_obj, "InvalidSubnets", "");
    // Succeeds because there are no Valid or Invalid subnets defined
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);
    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetIPv4Config", &parameters, &ret), 0);
    assert_int_equal(true, GET_BOOL(&ret, "Enable"));
    assert_string_equal("192.168.8.12", GET_CHAR(&ret, "IPAddress"));
    assert_string_equal("255.255.255.0", GET_CHAR(&ret, "SubnetMask"));
    assert_int_equal(true, GETP_BOOL(&ret, "DHCPParameters.Enable"));
    assert_int_equal(40000, GETP_UINT32(&ret, "DHCPParameters.LeaseTime"));
    assert_string_equal("192.168.8.249", GETP_CHAR(&ret, "DHCPParameters.MaxAddress"));
    assert_string_equal("192.168.8.100", GETP_CHAR(&ret, "DHCPParameters.MinAddress"));
    assert_string_equal("newpool", GETP_CHAR(&ret, "DHCPParameters.Pool"));
    assert_string_equal("192.168.8.12", GETP_CHAR(&ret, "DHCPParameters.DNSServers"));
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);
}

void test_alternative_ipv4_config(UNUSED void** state) {
    amxd_object_t* lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.");
    amxc_var_t ret;
    amxc_var_t parameters;

    assert_non_null(lan_obj);
    amxc_var_init(&ret);

    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(cstring_t, &parameters, "IPAddress", "172.10.10.6");
    amxc_var_add_key(cstring_t, &parameters, "SubnetMask", "255.255.0.0");
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetAlternativeIPv4Config", &parameters, &ret), amxd_status_ok);

    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetAlternativeIPv4Config", &parameters, &ret), amxd_status_ok);
    assert_int_equal(true, GET_BOOL(&ret, "Enable"));
    assert_string_equal("172.10.10.6", GET_CHAR(&ret, "IPAddress"));
    assert_string_equal("255.255.0.0", GET_CHAR(&ret, "SubnetMask"));

    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);
}

void test_ipv4_config_ext(UNUSED void** state) {
    amxd_object_t* lan_obj = amxd_dm_findf(amxut_bus_dm(), "Logical.Interface.lan.LAN.");
    amxc_var_t ret;
    amxc_var_t parameters;
    amxc_var_t param_copy;
    amxc_var_t* data = NULL;
    amxc_var_t* list = NULL;

    assert_non_null(lan_obj);
    amxc_var_init(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&param_copy);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    // Add IP configs
    list = amxc_var_add_key(amxc_llist_t, &parameters, "IPConfigs", NULL);

    // Add primary IP config
    data = amxc_var_add(amxc_htable_t, list, NULL);
    amxc_var_add_key(bool, data, "Enable", true);
    amxc_var_add_key(cstring_t, data, "IPAddress", "192.168.5.1");
    amxc_var_add_key(cstring_t, data, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(uint32_t, data, "Index", 1);

    // Add secondary IP config, missing Index
    data = amxc_var_add(amxc_htable_t, list, NULL);
    amxc_var_add_key(bool, data, "Enable", true);
    amxc_var_add_key(cstring_t, data, "IPAddress", "172.16.10.6");
    amxc_var_add_key(cstring_t, data, "SubnetMask", "255.255.0.0");

    // Add DHCP pool configs
    list = amxc_var_add_key(amxc_llist_t, &parameters, "Pools", NULL);

    // Add lan pool config, missing Alias
    data = amxc_var_add(amxc_htable_t, list, NULL);
    amxc_var_add_key(bool, data, "Enable", true);
    amxc_var_add_key(cstring_t, data, "MinAddress", "192.168.5.50");
    amxc_var_add_key(cstring_t, data, "MaxAddress", "192.168.5.200");
    amxc_var_add_key(cstring_t, data, "DNSServers", "192.168.5.1");
    amxc_var_add_key(uint32_t, data, "LeaseTime", 600);

    // Add lan-extender pool config
    data = amxc_var_add(amxc_htable_t, list, NULL);
    amxc_var_add_key(bool, data, "Enable", true);
    amxc_var_add_key(cstring_t, data, "Alias", "lan-extender");
    amxc_var_add_key(cstring_t, data, "MinAddress", "192.168.5.100");
    amxc_var_add_key(cstring_t, data, "MaxAddress", "192.168.5.150");
    amxc_var_add_key(cstring_t, data, "DNSServers", "192.168.5.1");
    amxc_var_add_key(uint32_t, data, "LeaseTime", 6000);

    assert_int_equal(amxc_var_copy(&param_copy, &parameters), 0);
    // Should fail, 2nd IP config index and lan pool Alias are missing
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4ConfigExt", &parameters, &ret), amxd_status_invalid_function_argument);

    // Add 2nd IP config index
    data = GETP_ARG(&param_copy, "IPConfigs.1.");
    amxc_var_add_key(uint32_t, data, "Index", 2);
    assert_int_equal(amxc_var_copy(&parameters, &param_copy), 0);
    // Should fail, lan pool Alias is still missing
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4ConfigExt", &parameters, &ret), amxd_status_invalid_function_argument);

    // Add lan pool Alias
    data = GETP_ARG(&param_copy, "Pools.0.");
    amxc_var_add_key(cstring_t, data, "Alias", "lan");
    assert_int_equal(amxc_var_copy(&parameters, &param_copy), 0);
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv4ConfigExt", &parameters, &ret), amxd_status_ok);

    // Verify the result
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);
    amxc_var_init(&ret);
    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetIPv4ConfigExt", &parameters, &ret), amxd_status_ok);
    // check contents
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);

    list = GET_ARG(&ret, "IPConfigs");
    assert_non_null(list);
    assert_true(amxc_var_type_of(list) == AMXC_VAR_ID_LIST);

    data = GETI_ARG(list, 0);
    assert_non_null(data);
    assert_true(GET_BOOL(data, "Enable"));
    assert_string_equal(GET_CHAR(data, "IPAddress"), "192.168.5.1");
    assert_string_equal(GET_CHAR(data, "SubnetMask"), "255.255.255.0");
    assert_int_equal(GET_UINT32(data, "Index"), 1);

    data = GETI_ARG(list, 1);
    assert_non_null(data);
    assert_true(GET_BOOL(data, "Enable"));
    assert_string_equal(GET_CHAR(data, "IPAddress"), "172.16.10.6");
    assert_string_equal(GET_CHAR(data, "SubnetMask"), "255.255.0.0");
    assert_int_equal(GET_UINT32(data, "Index"), 2);

    list = GET_ARG(&ret, "Pools");
    assert_non_null(list);
    assert_true(amxc_var_type_of(list) == AMXC_VAR_ID_LIST);

    data = GETI_ARG(list, 0);
    assert_non_null(data);
    assert_true(GET_BOOL(data, "Enable"));
    assert_string_equal(GET_CHAR(data, "Pool"), "lan");
    assert_string_equal(GET_CHAR(data, "MinAddress"), "192.168.5.50");
    assert_string_equal(GET_CHAR(data, "MaxAddress"), "192.168.5.200");
    assert_string_equal(GET_CHAR(data, "DNSServers"), "192.168.5.1");
    assert_int_equal(GET_UINT32(data, "LeaseTime"), 600);

    data = GETI_ARG(list, 1);
    assert_non_null(data);
    assert_true(GET_BOOL(data, "Enable"));
    assert_string_equal(GET_CHAR(data, "Pool"), "lan-extender");
    assert_string_equal(GET_CHAR(data, "MinAddress"), "192.168.5.100");
    assert_string_equal(GET_CHAR(data, "MaxAddress"), "192.168.5.150");
    assert_string_equal(GET_CHAR(data, "DNSServers"), "192.168.5.1");
    assert_int_equal(GET_UINT32(data, "LeaseTime"), 6000);

    amxc_var_clean(&parameters);
    amxc_var_clean(&param_copy);
    amxc_var_clean(&ret);
}

void test_ipv6_config(UNUSED void** state) {
    amxd_object_t* lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.");
    amxc_var_t ret;
    amxc_var_t parameters;

    assert_non_null(lan_obj);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetIPv6Config", &parameters, &ret), amxd_status_ok);
    assert_true(GET_BOOL(&ret, "Enable"));
    assert_true(GETP_BOOL(&ret, "DHCPParameters.Enable"));
    assert_string_equal("lan", GETP_CHAR(&ret, "DHCPParameters.Pool"));
    assert_false(GETP_BOOL(&ret, "DHCPParameters.IANAEnable"));
    assert_false(GETP_BOOL(&ret, "DHCPParameters.IANAEnable"));
    assert_true(GETP_BOOL(&ret, "RAParameters.RAEnable"));
    assert_string_equal("lan", GETP_CHAR(&ret, "RAParameters.RAPool"));
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &parameters, "Enable", true);
    amxc_var_add_key(bool, &parameters, "DHCPv6Enable", true);
    amxc_var_add_key(bool, &parameters, "IANAEnable", true);
    amxc_var_add_key(bool, &parameters, "IAPDEnable", false);
    amxc_var_add_key(cstring_t, &parameters, "DHCPv6Pool", "newpool");
    amxc_var_add_key(bool, &parameters, "RAEnable", true);
    amxc_var_add_key(cstring_t, &parameters, "RAPool", "newpool");
    assert_int_equal(amxd_object_invoke_function(lan_obj, "SetIPv6Config", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(lan_obj, "GetIPv6Config", &parameters, &ret), amxd_status_ok);
    assert_true(GET_BOOL(&ret, "Enable"));
    assert_true(GETP_BOOL(&ret, "DHCPParameters.Enable"));
    assert_true(GETP_BOOL(&ret, "DHCPParameters.IANAEnable"));
    assert_false(GETP_BOOL(&ret, "DHCPParameters.IAPDEnable"));
    assert_string_equal("newpool", GETP_CHAR(&ret, "DHCPParameters.Pool"));
    assert_true(GETP_BOOL(&ret, "RAParameters.RAEnable"));
    assert_string_equal("newpool", GETP_CHAR(&ret, "RAParameters.RAPool"));
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);
}

void test_lower_layer_config(UNUSED void** state) {
    amxc_var_t parameters;
    amxc_var_t ret;
    amxd_object_t* lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.lan.LAN.");
    amxd_object_t* guest_obj = amxd_dm_findf(amxut_bus_dm(), "%s", "Logical.Interface.guest.LAN.");

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.2.");
    // Fails because Device.Ethernet.Interface.2 is part of lan interface hierarchy
    assert_int_equal(amxd_object_invoke_function(guest_obj, "AddLowerLayerInterface", &parameters, &ret), amxd_status_invalid_function_argument);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.2.");
    // Fails because Device.Ethernet.Interface.2 is part of lan interface hierarchy
    assert_int_equal(amxd_object_invoke_function(guest_obj, "RemoveLowerLayerInterface", &parameters, &ret), amxd_status_invalid_function_argument);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.2.");
    assert_int_equal(amxd_object_invoke_function(lan_obj, "RemoveLowerLayerInterface", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.2.");
    assert_int_equal(amxd_object_invoke_function(guest_obj, "AddLowerLayerInterface", &parameters, &ret), amxd_status_ok);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.nonexisting");
    // Fails because Device.Ethernet.Interface.nonexisting doesn't exist
    assert_int_equal(amxd_object_invoke_function(guest_obj, "AddLowerLayerInterface", &parameters, &ret), amxd_status_object_not_found);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);

    amxc_var_init(&parameters);
    amxc_var_init(&ret);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "LowerLayerInterface", "Device.Ethernet.Interface.nonexisting");
    // Fails because Device.Ethernet.Interface.nonexisting doesn't exist
    assert_int_equal(amxd_object_invoke_function(guest_obj, "RemoveLowerLayerInterface", &parameters, &ret), amxd_status_object_not_found);
    amxc_var_clean(&parameters);
    amxc_var_clean(&ret);
}

void test_domain_search_list(UNUSED void** state) {
    amxc_var_t parameters;
    const char* lan_obj_string = "Logical.Interface.lan.LAN.";
    amxd_object_t* lan_obj = NULL;
    amxc_var_t option_params;
    char* ra_pool = NULL;
    const char* dhcpv4_pool = "DHCPv4Server.Pool.1.";
    const char* dhcpv6_pool = "DHCPv6Server.Pool.1.";
    amxd_trans_t trans;
    amxc_string_t dhcpv4_option;
    amxc_string_t dhcpv6_option;

    amxc_var_init(&option_params);

    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", lan_obj_string);
    assert_non_null(lan_obj);

    amxc_string_init(&dhcpv4_option, 0);
    amxc_string_setf(&dhcpv4_option, "%sOption.[Tag == '119'].", dhcpv4_pool);
    amxc_string_init(&dhcpv6_option, 0);
    amxc_string_setf(&dhcpv6_option, "%sOption.[Tag == '24'].", dhcpv6_pool);

    component_get_parameters(&option_params, amxc_string_get(&dhcpv6_option, 0), amxb_be_who_has("DHCPv6Server."));
    assert_true(GET_BOOL(&option_params, "Enable"));
    assert_string_equal(GET_CHAR(&option_params, "Value"),
                        "04686F6D65046172706100");

    ra_pool = component_get_path_instance(amxb_be_who_has("RouterAdvertisement."), "RouterAdvertisement.InterfaceSetting.[DNSSL == 'home.arpa']");
    assert_non_null(ra_pool);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, lan_obj);
    amxd_trans_set_value(cstring_t, &trans, "DomainSearchList", "be.softathome.com,rd.softathome.com,softathome.com");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    component_get_parameters(&option_params, amxc_string_get(&dhcpv4_option, 0), amxb_be_who_has("DHCPv4Server."));
    assert_true(GET_BOOL(&option_params, "Enable"));
    assert_string_equal(GET_CHAR(&option_params, "Value"),
                        "0262650A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D00");

    component_get_parameters(&option_params, amxc_string_get(&dhcpv6_option, 0), amxb_be_who_has("DHCPv6Server."));
    assert_true(GET_BOOL(&option_params, "Enable"));
    assert_string_equal(GET_CHAR(&option_params, "Value"),
                        "0262650A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D0004686F6D65046172706100");

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, lan_obj);
    amxd_trans_set_value(cstring_t, &trans, "DomainSearchList", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    component_get_parameters(&option_params, amxc_string_get(&dhcpv4_option, 0), amxb_be_who_has("DHCPv4Server."));
    assert_false(GET_BOOL(&option_params, "Enable"));
    assert_string_equal(GET_CHAR(&option_params, "Value"), "");

    component_get_parameters(&option_params, amxc_string_get(&dhcpv6_option, 0), amxb_be_who_has("DHCPv6Server."));
    assert_true(GET_BOOL(&option_params, "Enable"));
    assert_string_equal(GET_CHAR(&option_params, "Value"),
                        "04686F6D65046172706100");

    component_get_param(&option_params, ra_pool, amxb_be_who_has("RouterAdvertisement."), "DNSSL");
    assert_string_equal(GETP_CHAR(&option_params, "0.0.DNSSL"), "home.arpa");

    amxc_var_clean(&parameters);
    amxc_var_clean(&option_params);
    free(ra_pool);
    amxc_string_clean(&dhcpv4_option);
    amxc_string_clean(&dhcpv6_option);
}

void test_domain_name(UNUSED void** state) {
    amxc_var_t parameters;
    const char* lan_obj_string = "Logical.Interface.lan.LAN.";
    amxd_object_t* lan_obj = NULL;
    amxc_var_t option_params;
    const char* dhcpv4_pool = "DHCPv4Server.Pool.1.";
    const char* dns_config = "DNS.Relay.Config.1.";
    amxd_trans_t trans;

    amxc_var_init(&option_params);

    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    lan_obj = amxd_dm_findf(amxut_bus_dm(), "%s", lan_obj_string);
    assert_non_null(lan_obj);

    //include_dns();
    amxp_sigmngr_emit_signal(NULL, "wait:DNS.", NULL);
    expect_check_set_host("hostname-1", "Device.Logical.Interface.2.", "prplOS");
    amxut_bus_handle_events();

    component_get_param(&option_params, dhcpv4_pool, amxb_be_who_has("DHCPv4Server."), "DomainName");
    assert_string_equal(GETP_CHAR(&option_params, "0.0.DomainName"), "home.arpa");

    component_get_param(&option_params, dns_config, amxb_be_who_has("DNS."), "DomainName");
    assert_string_equal(GETP_CHAR(&option_params, "0.0.DomainName"), "home.arpa");

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, lan_obj);
    amxd_trans_set_value(cstring_t, &trans, "DomainName", "lan");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    component_get_param(&option_params, dhcpv4_pool, amxb_be_who_has("DHCPv4Server."), "DomainName");
    assert_string_equal(GETP_CHAR(&option_params, "0.0.DomainName"), "lan");

    component_get_param(&option_params, dns_config, amxb_be_who_has("DNS."), "DomainName");
    assert_string_equal(GETP_CHAR(&option_params, "0.0.DomainName"), "lan");

    amxc_var_clean(&parameters);
    amxc_var_clean(&option_params);
}
