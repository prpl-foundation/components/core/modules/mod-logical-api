/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __COMMON_UTILS_H__
#define __COMMON_UTILS_H__

#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <netmodel/common_api.h>

int component_get_param(amxc_var_t* ret_var, const char* component, amxb_bus_ctx_t* bus, const char* parameter);
int component_get_parameters(amxc_var_t* ret_var, const char* component, amxb_bus_ctx_t* bus);
char* component_get_path_instance(amxb_bus_ctx_t* bus, const char* query);
int component_set_str_param(const char* component, amxb_bus_ctx_t* bus, const char* param, const char* value);

typedef struct {
    bool include_dns;
} unittests_setup_t;

void mod_logical_unittests_setup(void** state, unittests_setup_t* ctx);
void mod_logical_unittests_teardown(void** state);

int check_expected_amxc_var_t(const LargestIntegralType value, const LargestIntegralType check_value_data);
void include_dns(void);
const char* skip_prefix(const char* path);

#define expect_check_set_host(alias, iface, name) do { \
        amxc_var_t* _args = NULL; \
        amxc_var_new(&_args); \
        amxc_var_set_type(_args, AMXC_VAR_ID_HTABLE); \
        amxc_var_add_key(cstring_t, _args, "Alias", alias); \
        amxc_var_add_key(bool, _args, "Enable", true); \
        amxc_var_add_key(bool, _args, "Exclusive", true); \
        amxc_var_add_key(cstring_t, _args, "IPAddresses", "192.168.1.1"); \
        amxc_var_add_key(cstring_t, _args, "Interface", iface); \
        amxc_var_add_key(cstring_t, _args, "Name", name); \
        amxc_var_add_key(bool, _args, "Persistent", true); \
        expect_check(_setHost, args, check_expected_amxc_var_t, _args); \
} while(0)

#define expect_check_set_host_name(alias, name) do { \
        amxc_var_t* _args = NULL; \
        amxc_var_new(&_args); \
        amxc_var_set_type(_args, AMXC_VAR_ID_HTABLE); \
        amxc_var_add_key(cstring_t, _args, "Alias", alias); \
        amxc_var_add_key(cstring_t, _args, "Name", name); \
        amxc_var_add_key(bool, _args, "Persistent", true); \
        expect_check(_setHost, args, check_expected_amxc_var_t, _args); \
} while(0)

#define expect_check_remove_host(alias) do { \
        amxc_var_t* _args = NULL; \
        amxc_var_new(&_args); \
        amxc_var_set_type(_args, AMXC_VAR_ID_HTABLE); \
        amxc_var_add_key(cstring_t, _args, "Alias", alias); \
        expect_check(_removeHost, args, check_expected_amxc_var_t, _args); \
} while(0)

#endif // __COMMON_UTILS_H__
