/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxc/amxc_macros.h>
#include <amxut/amxut_bus.h>

#include "mock_netmodel.h"
#include "common_utils.h"

amxc_var_t* __wrap_netmodel_getParameters(const char* intf,
                                          UNUSED const char* name,
                                          const char* flag,
                                          UNUSED const char* traverse) {
    amxc_var_t* ret = NULL;
    amxd_object_t* logical_obj = amxd_dm_findf(amxut_bus_dm(), intf);
    char* ip_intf = NULL;

    when_null(logical_obj, exit);
    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    ip_intf = amxd_object_get_value(cstring_t, logical_obj, "LowerLayers", NULL);

    if(strncmp(flag, "ip", 2) == 0) {
        amxc_var_add_key(cstring_t, ret, "IPInterface", ip_intf);
    } else if(strcmp(flag, "eth_link") == 0) {
        amxc_var_t eth_link_intf;
        amxc_var_init(&eth_link_intf);

        component_get_param(&eth_link_intf, skip_prefix(ip_intf), amxb_be_who_has("IP."), "LowerLayers");
        amxc_var_add_key(cstring_t, ret, "EthernetLinkInterface", GETP_CHAR(&eth_link_intf, "0.0.LowerLayers"));
        amxc_var_clean(&eth_link_intf);
    } else if(strcmp(flag, "bridge") == 0) {
        amxc_var_t eth_link_intf;
        amxc_var_t bridge_intf;
        amxc_var_init(&eth_link_intf);
        amxc_var_init(&bridge_intf);

        component_get_param(&eth_link_intf, skip_prefix(ip_intf), amxb_be_who_has("IP."), "LowerLayers");
        component_get_param(&bridge_intf, skip_prefix(GETP_CHAR(&eth_link_intf, "0.0.LowerLayers")), amxb_be_who_has("Bridging."), "LowerLayers");

        amxc_var_add_key(cstring_t, ret, "BridgeInterface", GETP_CHAR(&bridge_intf, "0.0.LowerLayers"));
        amxc_var_clean(&eth_link_intf);
        amxc_var_clean(&bridge_intf);
    } else if(strcmp(flag, "gpon") == 0) {
        // GPON is stubbed (has not been tested)
    } else if((strncmp(flag, "dhcpv", 5) == 0) || (strcmp(flag, "ppp-up") == 0)) {
        amxc_string_t search_path;
        amxc_var_t ipv4_addr;
        amxc_var_t* addressing_type = NULL;

        amxc_string_init(&search_path, 0);
        amxc_string_setf(&search_path, "%sIPv4Address.1.", skip_prefix(ip_intf));
        amxc_var_init(&ipv4_addr);

        component_get_parameters(&ipv4_addr, amxc_string_get(&search_path, 0), amxb_be_who_has("IP."));

        addressing_type = amxc_var_get_key(&ipv4_addr, "AddressingType", AMXC_VAR_FLAG_DEFAULT);
        const char* intf_type = amxc_var_constcast(cstring_t, addressing_type);

        if((strcmp(intf_type, "DHCP") == 0) && (strncmp(flag, "dhcpv", 5) == 0)) {
            amxc_var_add_key(cstring_t, ret, "DHCPInterface", ip_intf);
        } else if((strcmp(intf_type, "Static") == 0) && (strcmp(flag, "ppp-up") == 0)) {
            amxc_var_add_key(cstring_t, ret, "PPPInterface", ip_intf);
        }

        amxc_var_clean(&ipv4_addr);
        amxc_string_clean(&search_path);
    }

exit:
    free(ip_intf);
    return ret;
}

amxc_var_t* __wrap_netmodel_getFirstParameter(const char* intf,
                                              const char* name,
                                              const char* flag,
                                              const char* traverse) {
    amxc_var_t* ret = NULL;

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    if(strcmp(name, "Status") == 0) {
        amxc_var_add(bool, ret, true);
    } else if((strcmp(name, "ConnectionStatus") == 0) && (strcmp(flag, "ppp-up") == 0)) {
        amxc_var_add(cstring_t, ret, "Connected");
    } else if((strcmp(name, "DHCPStatus") == 0) && (strncmp(flag, "dhcpv", 5) == 0)) {
        amxc_var_add(cstring_t, ret, "Bound");
    } else if((strcmp(name, "LastConnectionError") == 0) && (strcmp(flag, "ppp-up") == 0)) {
        amxc_var_add(cstring_t, ret, "ERROR_NONE");
    } else if((strcmp(name, "IPRouters") == 0) && (strcmp(flag, "ipv4-up") == 0)) {
        amxc_var_add(cstring_t, ret, "7.7.7.7");
    } else if((strcmp(name, "InterfacePath") == 0)) {
        amxc_var_t* params = __wrap_netmodel_getParameters(intf, name, flag, traverse);
        amxc_var_copy(ret, amxc_var_get_first(params));
        amxc_var_delete(&params);
    }


    return ret;
}

amxc_var_t* __wrap_netmodel_getAddrs(UNUSED const char* const interface,
                                     const char* const flag,
                                     UNUSED const char* const traverse) {
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_t* ip = amxc_var_add(amxc_htable_t, ret, NULL);

    if(strcmp(flag, "ipv4") == 0) {
        amxc_var_add_key(cstring_t, ip, "Family", "ipv4");
        amxc_var_add_key(cstring_t, ip, "Address", "235.235.235.235");
        amxc_var_add_key(cstring_t, ip, "Peer", "8.8.8.8");
    }

    if(strcmp(flag, "ipv6") == 0) {
        amxc_var_add_key(cstring_t, ip, "Family", "ipv6");
        amxc_var_add_key(cstring_t, ip, "Address", "2804::2804::8");
        amxc_var_add_key(cstring_t, ip, "TypeFlags", "@gua");
    }

    if((strcmp(flag, "@private || @gua") == 0) || (strcmp(flag, "@private") == 0)) {
        amxc_var_add_key(cstring_t, ip, "Address", "192.168.1.1"); // for hostname
    }

    return ret;
}

amxc_var_t* __wrap_netmodel_getFirstIPv6Prefix(UNUSED const char* intf,
                                               UNUSED const char* flag,
                                               UNUSED const char* traverse) {
    amxc_var_t* ret = NULL;

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, ret, "Prefix", "2000:bee:bee:bee::/48");
    amxc_var_add_key(cstring_t, ret, "Origin", "PrefixDelegation");
    amxc_var_add_key(cstring_t, ret, "StaticType", "PrefixDelegation");

    return ret;
}

amxc_var_t* __wrap_netmodel_getIntfs(const char* const intf,
                                     const char* const flag,
                                     const char* const traverse) {
    amxc_var_t* ret;

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    assert_non_null(intf);
    assert_non_null(flag);
    assert_non_null(traverse);

    if(strcmp(flag, "ipv4") == 0) {
        if(strcmp(intf, "wan4") == 0) {        // Should match with the wan_ppp instance in mock_logical_defaults.odl
            amxc_var_add(cstring_t, ret, "ip-wan_ppp");
        } else if(strcmp(intf, "wan5") == 0) { // Should match with the wan_dhcp instance in mock_logical_defaults.odl
            amxc_var_add(cstring_t, ret, "ip-wan_dhcp");
        } else if(strcmp(intf, "wan6") == 0) { // Should match with the wan_static instance in mock_logical_defaults.odl
            amxc_var_add(cstring_t, ret, "ip-wan_static");
        }
    } else if(strcmp(flag, "ipv6") == 0) {
        if(strcmp(intf, "wan4") == 0) {        // Should match with the wan_ppp instance in mock_logical_defaults.odl
            amxc_var_add(cstring_t, ret, "ip-wan_dhcp");
        } else if(strcmp(intf, "wan5") == 0) { // Should match with the wan_dhcp instance in mock_logical_defaults.odl
            amxc_var_add(cstring_t, ret, "ip-wan_ppp");
        }
    }

    return ret;
}

bool __wrap_netmodel_hasFlag(UNUSED const char* intf, const char* flag, UNUSED const char* condition, UNUSED const char* traverse) {
    bool rv = false;

    assert_non_null(intf);
    assert_non_null(flag);
    assert_non_null(traverse);
    assert_string_equal(traverse, "down");

    if((strcmp(intf, "ip-wan_ppp") == 0) && (strcmp(flag, "ppp") == 0)) {
        rv = true;
    } else if((strcmp(intf, "ip-wan_dhcp") == 0) && ((strcmp(flag, "dhcpv4") == 0) || (strcmp(flag, "dhcpv6") == 0))) {
        rv = true;
    } else if((strcmp(intf, "ip-wan_static") == 0) && (strcmp(flag, "ipv4-up") == 0)) {
        rv = true;
    }

    return rv;
}
