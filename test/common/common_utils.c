/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <netmodel/client.h>

#include "common_utils.h"
#include "mock_netmodel.h"

int component_get_param(amxc_var_t* ret_var, const char* component, amxb_bus_ctx_t* bus, const char* parameter) {
    int rc = -1;
    amxc_string_t param_path;

    amxc_string_init(&param_path, 0);

    amxc_string_setf(&param_path, "%s%s", component, parameter);

    rc = amxb_get(bus, amxc_string_get(&param_path, 0), 0, ret_var, 3);
    assert_int_equal(rc, 0);

    amxc_string_clean(&param_path);
    return rc;
}

static int multiple_components_get_parameters(amxc_var_t* ret_var, const char* component, amxb_bus_ctx_t* bus) {
    int rc = -1;

    rc = amxb_get(bus, component, 0, ret_var, 3);
    assert_int_equal(rc, 0);

    return rc;
}

int component_get_parameters(amxc_var_t* ret_var, const char* component, amxb_bus_ctx_t* bus) {
    int rc = -1;
    amxc_var_t result;

    amxc_var_init(&result);

    rc = multiple_components_get_parameters(&result, component, bus);
    assert_int_equal(rc, 0);
    amxc_var_move(ret_var, GETP_ARG(&result, "0.0"));

    amxc_var_clean(&result);
    return rc;
}

char* component_get_path_instance(amxb_bus_ctx_t* bus, const char* query) {
    amxc_var_t ret;
    const char* result = NULL;
    char* ret_str = NULL;

    amxc_var_init(&ret);
    assert_int_equal(amxb_get(bus, query, 0, &ret, 5), 0);
    result = amxc_var_key(GETP_ARG(&ret, "0.0"));
    ret_str = strdup(result);
    assert_false((ret_str == NULL) || (*ret_str == '\0'));

    amxc_var_clean(&ret);
    return ret_str;
}

static int component_set_params(const char* component, amxb_bus_ctx_t* bus, amxc_var_t* values) {
    amxc_var_t ret;
    int rc = -1;

    amxc_var_init(&ret);

    rc = amxb_set(bus, component, values, &ret, 5);
    assert_int_equal(rc, 0);

    amxc_var_clean(&ret);
    return rc;
}

int component_set_str_param(const char* component, amxb_bus_ctx_t* bus, const char* param, const char* value) {
    int rc = -1;
    amxc_var_t parameters;

    amxc_var_init(&parameters);

    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, param, value);

    rc = component_set_params(component, bus, &parameters);

    amxc_var_clean(&parameters);
    return rc;
}

typedef struct {
    const char* path;
    const char* netmodel_alias; // ipv4 and ipv6 can be different, see __wrap_netmodel_getIntfs
} mock_netmodel_map_t;

// make sure to also add instance in mod_netmodel.odl or else the getParameters (or another func) won't be called even if resolvePath returned something correct
static mock_netmodel_map_t mock_netmodel_map[] = {
    {"Logical.Interface.1.", "wan"},
    {"Logical.Interface.2.", "lan"},
    {"Logical.Interface.3.", "guest"},
    {"Device.Ethernet.Link.2.", "ethIntf-ETH1"},
    {"Logical.Interface.4.", "wan4"}, // so __wrap_netmodel_getIntfs can make the diff
    {"Logical.Interface.5.", "wan5"}, // so __wrap_netmodel_getIntfs can make the diff
    {"Logical.Interface.6.", "wan6"}, // so __wrap_netmodel_getIntfs can make the diff
    {NULL, NULL}
};

static amxc_var_t* openquery_args = NULL;

static mock_netmodel_map_t* find_mock_netmodel_map(const char* path, const char* netmodel_alias) {
    mock_netmodel_map_t* map = NULL;
    for(int i = 0; mock_netmodel_map[i].path != NULL; i++) {
        if((path != NULL) && (strcmp(path, mock_netmodel_map[i].path) == 0)) {
            map = &mock_netmodel_map[i];
            break;
        }
        if((netmodel_alias != NULL) && (strcmp(netmodel_alias, mock_netmodel_map[i].netmodel_alias) == 0)) {
            map = &mock_netmodel_map[i];
            break;
        }
    }
    assert_non_null(map); // if new, add entry to mock_netmodel_map
    return map;
}

static amxd_status_t _resolvePath(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    mock_netmodel_map_t* map = NULL;
    map = find_mock_netmodel_map(GETP_CHAR(args, "path"), NULL);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, map->netmodel_alias);
    return amxd_status_ok;
}

static amxd_status_t _openQuery(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_var_set(uint32_t, ret, 1); // don't forget to add Query instance in mock_netmodel.odl
    assert_null(openquery_args);
    amxc_var_new(&openquery_args);
    amxc_var_copy(openquery_args, args);
    return amxd_status_ok;
}

static amxd_status_t _closeQuery(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _getIntfs(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    result = __wrap_netmodel_getIntfs(amxd_object_get_name(object, AMXD_OBJECT_NAMED), GET_CHAR(args, "flag"), GET_CHAR(args, "traverse"));
    assert_non_null(result);
    amxc_var_move(ret, result);
    amxc_var_delete(&result);
    return amxd_status_ok;
}

static amxd_status_t _getParameters(amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    mock_netmodel_map_t* map = NULL;
    map = find_mock_netmodel_map(NULL, amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    // still using the __wrap_ because I didn't want to mess with it
    result = __wrap_netmodel_getParameters(map->path, GET_CHAR(args, "name"), GET_CHAR(args, "flag"), GET_CHAR(args, "traverse"));
    assert_non_null(result);
    amxc_var_move(ret, result);
    amxc_var_delete(&result);
    return amxd_status_ok;
}

static amxd_status_t _getFirstParameter(amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        amxc_var_t* args,
                                        amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    mock_netmodel_map_t* map = NULL;
    map = find_mock_netmodel_map(NULL, amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    result = __wrap_netmodel_getFirstParameter(map->path, GET_CHAR(args, "name"), GET_CHAR(args, "flag"), GET_CHAR(args, "traverse"));
    assert_non_null(result);
    amxc_var_move(ret, result);
    amxc_var_delete(&result);
    return amxd_status_ok;
}

static amxd_status_t _getAddrs(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    result = __wrap_netmodel_getAddrs(amxd_object_get_name(object, AMXD_OBJECT_NAMED), GET_CHAR(args, "flag"), GET_CHAR(args, "traverse"));
    assert_non_null(result);
    amxc_var_move(ret, result);
    amxc_var_delete(&result);
    return amxd_status_ok;
}

static amxd_status_t _hasFlag(amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    bool result = false;
    result = __wrap_netmodel_hasFlag(amxd_object_get_name(object, AMXD_OBJECT_NAMED), GET_CHAR(args, "flag"), GET_CHAR(args, "condition"), GET_CHAR(args, "traverse"));
    amxc_var_set(bool, ret, result);
    return amxd_status_ok;
}

static amxd_status_t _getFirstIPv6Prefix(UNUSED amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         UNUSED amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    result = __wrap_netmodel_getFirstIPv6Prefix(amxd_object_get_name(object, AMXD_OBJECT_NAMED), GET_CHAR(args, "flag"), GET_CHAR(args, "traverse"));
    assert_non_null(result);
    amxc_var_move(ret, result);
    amxc_var_delete(&result);
    return amxd_status_ok;
}

static amxd_status_t _getResult(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    int rv = amxd_status_ok;
    const char* class = NULL;
    amxd_object_t* netmodel_intf = amxd_object_findf(object, ".^.^");
    assert_non_null(netmodel_intf);
    assert_non_null(openquery_args); // if NULL, is openQuery called?
    class = GET_CHAR(openquery_args, "class");
    assert_non_null(class);

    if(strcmp(class, "getAddrs") == 0) {
        rv = _getAddrs(netmodel_intf, NULL, openquery_args, ret);
    } else if(strcmp(class, "getFirstParameter") == 0) {
        rv = _getFirstParameter(netmodel_intf, NULL, openquery_args, ret);
    } else {
        bool* unknown_query_class = NULL; // supposed to trigger next assert
        assert_non_null(unknown_query_class);
    }

    amxc_var_delete(&openquery_args);
    return rv;
}

static amxd_status_t _setHost(UNUSED amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              UNUSED amxc_var_t* args,
                              amxc_var_t* ret) {
    check_expected(args);
    amxc_var_set(cstring_t, ret, "dummy");
    return amxd_status_ok;
}

static amxd_status_t _removeHost(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    check_expected(args);
    return amxd_status_ok;
}

static void include_odl(const char* odl_file) {
    amxc_string_t odl;
    amxo_parser_t* parser = NULL;
    int rv = -1;

    parser = amxut_bus_parser();

    amxc_string_init(&odl, 0);
    amxc_string_set(&odl, "%config{prefix_=\"\"; import-dirs=[\".\",\"../../output/x86_64-linux-gnu/coverage/\"]; include-dirs=[\".\",\"../../odl\",\"../common/odl\"];} include \"");
    amxc_string_appendf(&odl, "%s\";", odl_file);
    rv = amxo_parser_parse_string(parser, amxc_string_get(&odl, 0), amxd_dm_get_root(amxut_bus_dm()));
    if(0 != rv) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&parser->msg, 0));
    }
    amxc_string_clean(&odl);
}

void include_dns(void) {
    include_odl("mock_dns.odl");
}

void mod_logical_unittests_setup(void** state, unittests_setup_t* ctx) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();

    amxp_sigmngr_add_signal(NULL, "wait:DNS.");
    amxp_sigmngr_add_signal(NULL, "wait:done");

    // cannot use __wrap_xyz because mod-logical-api is compiled with other linker flags
    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "resolvePath", AMXO_FUNC(_resolvePath));
    amxo_resolver_ftab_add(parser, "getIntfs", AMXO_FUNC(_getIntfs));
    amxo_resolver_ftab_add(parser, "getParameters", AMXO_FUNC(_getParameters));
    amxo_resolver_ftab_add(parser, "getFirstParameter", AMXO_FUNC(_getFirstParameter));
    amxo_resolver_ftab_add(parser, "getAddrs", AMXO_FUNC(_getAddrs));
    amxo_resolver_ftab_add(parser, "hasFlag", AMXO_FUNC(_hasFlag));
    amxo_resolver_ftab_add(parser, "getFirstIPv6Prefix", AMXO_FUNC(_getFirstIPv6Prefix));

    amxo_resolver_ftab_add(parser, "SetHost", AMXO_FUNC(_setHost));
    amxo_resolver_ftab_add(parser, "RemoveHost", AMXO_FUNC(_removeHost));

    include_odl("mock_root.odl");
    if(ctx->include_dns) {
        expect_check_set_host("hostname-1", "Device.Logical.Interface.2.", "prplOS");
        include_dns();
    }

    assert_int_equal(amxo_parser_invoke_entry_points(parser, amxut_bus_dm(), 0), 0);

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    amxut_bus_handle_events();
}

void mod_logical_unittests_teardown(void** state) {
    amxd_dm_t* dm = NULL;
    dm = amxut_bus_dm();
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:stop", NULL);
    assert_int_equal(amxo_parser_invoke_entry_points(amxut_bus_parser(), dm, 1), 0);
    netmodel_cleanup();
    amxm_close_all();
    amxut_bus_teardown(state);
    // Handle events that are generated by deleting the data model.
    amxut_bus_handle_events();
}

int check_expected_amxc_var_t(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* a = (amxc_var_t*) value;
    amxc_var_t* b = (amxc_var_t*) check_value_data;
    int result = -1;
    int rv = 0;
    print_message("from plugin\n");
    amxc_var_dump(a, 1);
    print_message("from unit test\n");
    amxc_var_dump(b, 1);
    if((amxc_var_compare(a, b, &result) != 0) ||
       (result != 0)) {
        rv = 0;
        goto exit;
    }
    rv = 1;
exit:
    amxc_var_delete(&b);
    return rv;
}

const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    when_str_empty(path, exit);

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }

exit:
    return path;
}
