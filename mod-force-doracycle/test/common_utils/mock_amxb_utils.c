/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <amxut/amxut_bus.h>

#include <amxc/amxc_macros.h>
#include "mock_amxb_utils.h"

// Local variables
bool bool_eth = false;
bool bool_wifi = false;

// Wrapped so we do not need to mock NetModel and we can just return the bus ctx
amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* object_path) {
    assert_non_null(object_path);
    return amxut_bus_ctx();
}

int __wrap_amxb_get(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    int32_t depth,
                    amxc_var_t* ret,
                    int timeout) {
    amxc_var_t* list = NULL;
    amxc_var_t* object_var = NULL;

    assert_non_null(bus_ctx);
    assert_non_null(object);
    assert_int_equal(depth, 0);
    assert_non_null(ret);
    assert_false(timeout < 1);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    if(strncmp("NetModel.Intf.", object, 14) == 0) {
        list = amxc_var_add(amxc_llist_t, ret, NULL);
        object_var = amxc_var_add(amxc_htable_t, list, NULL);
        if(strstr(object, "wifi") == NULL) {
            amxc_var_add_key(cstring_t, object_var, "InterfacePath", "Device.Ethernet.Link.dummy-ethernet.");
        } else {
            amxc_var_add_key(cstring_t, object_var, "InterfacePath", "Device.WiFi.SSID.dummy-wifi.");
        }
        return 0;
    }
    return 1;
}
