/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netmodel/client.h"

#include "mod_force_doracycle.h"
#include <string.h>
#define ME "logical"

/**
 * @brief
 * Returns the given string without the "Device." prefix
 *
 * @param path The string from which the "Device." prefix should be removed
 * @return Pointer to the beginning of the string without Device., or path if the given path is empty
 */
static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";
    size_t len = strlen(prefix);

    when_str_empty(path, exit);

    if(strncmp(path, prefix, len) == 0) {
        path += len;
    }

exit:
    return path;
}

/**
 * @brief
 * Callback function of the timer to enable back the lowermost interface.
 *
 * @param local_timer Timer that called the function
 * @param priv The data to find the lowermost interface
 */
static void enable_intf_delayed(amxp_timer_t* local_timer,
                                void* priv) {
    int r = 0;
    amxc_var_t param;
    amxc_var_t ret;
    amxc_var_t* data = priv;
    amxb_bus_ctx_t* ctx = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&param);
    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    when_null_trace(data, exit, ERROR, "No data given in delayed enable interface");

    ctx = amxb_be_who_has(GET_CHAR(data, "path"));
    when_null_trace(ctx, exit, ERROR, "No bus found for the interface to enable");

    amxc_var_add_key(bool, &param, "Enable", true);

    r = amxb_set(ctx, GET_CHAR(data, "path"), &param, &ret, 10);
    when_failed_trace(r, exit, ERROR, "Problem setting lowermost interface to enable : %s %d", GET_CHAR(data, "path"), r);
exit:
    amxp_timer_delete(&local_timer);
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
    amxc_var_clean(&param);
    return;
}

/**
 * @brief
 * Callback function to enable again an interface that was down.
 * This function also remove the down subscription of the interface previously created.
 *
 * @param sig_name Name of the signal
 * @param data Data of the event
 * @param priv NULL
 */
static void enable_intf(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    int r = -1;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t* cpy_data = NULL;
    amxp_timer_t* local_timer = NULL;

    amxc_var_new(&cpy_data);

    when_null_trace(data, exit, ERROR, "No data given to enable back interface");

    r = amxc_var_copy(cpy_data, data);
    when_failed_trace(r, exit, ERROR, "Cannot copy data event");
    ctx = amxb_be_who_has(GET_CHAR(data, "path"));
    when_null_trace(ctx, exit, ERROR, "No bus found for the interface to enable");

    r = amxp_timer_new(&local_timer, enable_intf_delayed, (void*) cpy_data);
    when_failed_trace(r, exit, ERROR, "Cannot create timer");
    // A timer is needed before enabling the interface, as we toggle it too fast for the interface of the client to detect it.
    r = amxp_timer_start(local_timer, 10000);
    when_failed_trace(r, exit, ERROR, "Cannot start timer");
    if(amxb_unsubscribe(ctx, GET_CHAR(data, "path"), enable_intf, NULL) != 0) {
        // Failing to subscribe should not change the r value
        // Timer callback will be called, data should not be deleted
        SAH_TRACEZ_ERROR(ME, "Unable to delete the down subscription");
    }

exit:
    if(r != 0) {
        amxc_var_delete(&cpy_data);
    }
    return;
}

/**
 * @brief
 * Create a subscription to an interface to know when then link is down to bring it up again afterward
 *
 * @param intf_bus Bus of the lowermost interface
 * @param intf_path Path of the lowermost interface to subscribe to
 * @return int 0 if subscription was successful
 */
static int create_down_subscription(amxb_bus_ctx_t* intf_bus, const char* intf_path) {
    int ret = 1;
    when_null_trace(intf_bus, exit, ERROR, "No bus given for the lowermost interface");
    when_str_empty_trace(intf_path, exit, ERROR, "No interface path given for subscription");
    ret = amxb_subscribe(intf_bus,
                         intf_path,
                         "notification == 'dm:object-changed' && parameters.Status.to == 'Down'",
                         enable_intf,
                         NULL);
    when_failed_trace(ret, exit, ERROR, "Unable to create down subscription for interface %s", intf_path);
exit:
    return ret;
}

/**
 * @brief
 * Toggle an interface.
 *
 * @param netmodel_bus Bus that contains the NetModel dm
 * @param intf_bus Bus that contains the lowermost interface Enable parameter
 * @param netmodel_intf_alias Alias of the lowermost interface in the NetModel dm
 * @return int 0 if toggle succeed
 */
static int toggle_intf(amxb_bus_ctx_t* netmodel_bus, amxb_bus_ctx_t* intf_bus, const char* netmodel_intf_alias) {
    int r = 1;
    amxc_string_t path;
    amxc_var_t ret;
    amxc_var_t param;
    const char* intf_path = NULL;

    amxc_var_init(&param);
    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    when_null_trace(netmodel_bus, exit, ERROR, "No bus for NetModel given");
    when_null_trace(intf_bus, exit, ERROR, "No bus to toggle the lowermost interface");
    when_str_empty_trace(netmodel_intf_alias, exit, ERROR, "No netmodel interface alias given for lowermost interface");

    r = amxc_string_setf(&path, "NetModel.Intf.[Alias=='%s'].", netmodel_intf_alias);
    when_failed_trace(r, exit, ERROR, "Problem setting alias path string for NetModel");
    r = amxb_get(netmodel_bus, amxc_string_get(&path, 0), 0, &ret, 10);
    when_failed_trace(r, exit, ERROR, "Problem getting NetModel data for the interface");
    intf_path = skip_prefix(GETP_CHAR(&ret, "0.0.InterfacePath"));
    when_str_empty_trace(intf_path, exit, ERROR, "Problem getting the interface path");

    r = create_down_subscription(intf_bus, intf_path);
    when_failed_trace(r, exit, ERROR, "Cannot create down subscription");

    amxc_var_add_key(bool, &param, "Enable", false);

    r = amxb_set(intf_bus, intf_path, &param, &ret, 10);
    when_failed_trace(r, exit, ERROR, "Problem setting lowermost interface to disable : %d", r);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&param);
    amxc_string_clean(&path);
    return r;
}

/**
 * @brief
 * Toggle all the interface where their netmodel name is in the list.
 *
 * @param list_intf list of netmodel interface name
 * @param dm_entry Lower layer data model where to find the interface controller
 * @return int 0 if all toggle succeed
 */
static int toggle_intfs(amxc_var_t* list_intf, const char* dm_entry) {
    int r = 1;
    amxb_bus_ctx_t* netmodel_ctx = netmodel_get_amxb_bus();
    amxb_bus_ctx_t* ctx = NULL;

    when_str_empty_trace(dm_entry, exit, ERROR, "No data model entry given");
    when_null_trace(list_intf, exit, ERROR, "No list of netmodel name given");
    when_null_trace(netmodel_ctx, exit, ERROR, "No bus found for NetModel");

    ctx = amxb_be_who_has(dm_entry);
    when_null_trace(ctx, exit, ERROR, "No bus found for lowermost interface controller %s", dm_entry);

    amxc_var_for_each(intf, list_intf) {
        r = toggle_intf(netmodel_ctx, ctx, GET_CHAR(intf, NULL));
        when_failed_trace(r, exit, ERROR, "Failed to toggle intf %s", GET_CHAR(intf, NULL));
    }
    r = 0;
exit:
    return r;
}

/**
 * @brief
 * For the lowermost interface to toggle
 *
 * @param function_name unused
 * @param args htable that contains the uppermost interface
 * @param ret unused
 * @return return 0 if succeed, 1 elsewhere
 */
int force_lowermost_layer_toggle(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    int r = 1;
    const char* intf_path = NULL;
    amxc_var_t* list_intf = NULL;

    when_null_trace(args, exit, ERROR, "No argument given");
    intf_path = GET_CHAR(args, "Interface");
    when_str_empty_trace(intf_path, exit, ERROR, "No interface path given");

    list_intf = netmodel_getIntfs(intf_path, "up && eth_intf", netmodel_traverse_down);
    r = toggle_intfs(list_intf, "Ethernet.");
    amxc_var_delete(&list_intf);
    when_failed_trace(r, exit, ERROR, "Toggle of Ethernet lowermost layer failed");

    list_intf = netmodel_getIntfs(intf_path, "up && ssid", netmodel_traverse_down);
    r = toggle_intfs(list_intf, "WiFi.");
    amxc_var_delete(&list_intf);
    when_failed_trace(r, exit, ERROR, "Toggle of WiFi lowermost layer failed");
exit:
    return r;
}

/**
 * @brief
 * Start and register functions for this module.
 *
 * @return AMXM_CONSTRUCTOR return 0 if succeed.
 */
static AMXM_CONSTRUCTOR dhcp_utils_start(void) {
    int r = 0;
    amxm_module_t* mod = NULL;
    amxm_shared_object_t* so = amxm_so_get_current();
    r |= amxm_module_register(&mod, so, MOD_FORCE);
    r |= amxm_module_add_function(mod, "force_lowermost_layer_toggle", force_lowermost_layer_toggle);
    return r;
}
