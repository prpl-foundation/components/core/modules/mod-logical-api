# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.8.11 - 2024-12-19(13:23:18 +0000)

### Other

- DNS domain name can no longer be set after data model changes

## Release v0.8.10 - 2024-12-06(13:29:36 +0000)

### Other

- [TR181-Logical] Avoid using Device. prefix when not

## Release v0.8.9 - 2024-11-07(17:50:29 +0000)

### Other

- [Logical] start logical plugin when dhcpvx server are not present

## Release v0.8.8 - 2024-10-03(07:42:02 +0000)

### Other

- - [Terminating dot][TR181] Fix Bridging Component

## Release v0.8.7 - 2024-08-13(10:41:12 +0000)

### Other

- add support for multiple runlevels

## Release v0.8.6 - 2024-07-23(06:56:18 +0000)

## Release v0.8.5 - 2024-07-12(08:18:11 +0000)

### Other

- Device.IP.Interface.wan.Status and Device.Logical.Interface.wan.Status are not in Sync

## Release v0.8.4 - 2024-06-17(15:32:14 +0000)

### Other

- DomainName should be supported and should also be used for DNS suffix

## Release v0.8.3 - 2024-05-23(13:04:29 +0000)

### Other

- [WAN] GetWANStatus relies on IPv6 netmodel flag iprouter-up to...

## Release v0.8.2 - 2024-02-05(13:32:30 +0000)

### Fixes

- [LAN manager] Issue with IPRouters and DHCPv4PoolReference when configuring multiple dhcp pools

## Release v0.8.1 - 2024-01-25(14:27:34 +0000)

### Other

- Set default DHCP pool references in logical API

## Release v0.8.0 - 2023-12-22(08:53:00 +0000)

### New

- [LAN manager] Add support for configuring multiple IP configs and DHCP pools

## Release v0.7.1 - 2023-12-19(11:23:01 +0000)

### Fixes

- tr181-logical fails to set DNS hostname at startup

## Release v0.7.0 - 2023-12-07(12:35:30 +0000)

### New

- Add ipv6 Server information in the WAN Status call

## Release v0.6.5 - 2023-12-07(10:40:42 +0000)

### Fixes

- [WAN]GetWANStatus not working after updating data using WANManager Static Mode

## Release v0.6.4 - 2023-11-27(14:17:42 +0000)

### Fixes

- GetWANStatus not working after updating data using WANManager

## Release v0.6.3 - 2023-11-24(15:47:12 +0000)

### Fixes

-  [LANManager][LAN MIB] The validation of the allowed subnets does not work correctly

## Release v0.6.2 - 2023-11-07(13:42:48 +0000)

### Other

- Fix syntax in gitlab-ci file

## Release v0.6.1 - 2023-10-10(11:35:15 +0000)

### Other

- [LANManager][LAN MIB] Only allow class A, B, C addresses on the LAN

## Release v0.6.0 - 2023-09-07(10:17:10 +0000)

### New

- Move the force dora cycle functionality

## Release v0.5.0 - 2023-08-29(09:24:53 +0000)

### New

- [Logical] GetWANStatus() API not working

## Release v0.4.1 - 2023-08-04(14:24:23 +0000)

### Other

- Opensource component

## Release v0.4.0 - 2023-07-17(12:27:08 +0000)

### New

- [tr181-logical][LAN mib] The Lan mib must expose the domainSearchList parameter

## Release v0.3.0 - 2023-06-15(08:22:40 +0000)

### New

- [tr181-logical][WAN mib] The Wan mib must expose the ipv4 address in the DM.

## Release v0.2.1 - 2023-05-10(07:58:48 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.2.0 - 2023-05-08(07:06:48 +0000)

### New

- [mod-logical-api][TR181-Logical] Implement an API for creating and configuring subnets

## Release v0.1.0 - 2023-03-24(08:49:46 +0000)

### New

- Model LANManager functionality in TR181-Logical

## Release 0.0.0 - 2023-02-08(12:18:32 +0000)


