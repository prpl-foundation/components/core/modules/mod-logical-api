include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-force-doracycle/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C mod-force-doracycle/src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-force-doracycle.so $(DEST)/usr/lib/amx/modules/mod-force-doracycle.so
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api.odl $(DEST)/etc/amx/tr181-logical/extensions/mod-logical-api.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-lan.odl $(DEST)/etc/amx/tr181-logical/mod-logical-api-lan.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-wan.odl $(DEST)/etc/amx/tr181-logical/mod-logical-api-wan.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-subnet.odl $(DEST)/etc/amx/tr181-logical/mod-logical-api-subnet.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-dependencies.odl $(DEST)/etc/amx/tr181-logical/mod-logical-api-dependencies.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/tr181-logical/defaults.gw.d
	$(foreach odl,$(wildcard odl/defaults.gw.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/tr181-logical/defaults.gw.d/;)
endif

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-force-doracycle.so $(PKGDIR)/usr/lib/amx/modules/mod-force-doracycle.so
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api.odl $(PKGDIR)/etc/amx/tr181-logical/extensions/mod-logical-api.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-lan.odl $(PKGDIR)/etc/amx/tr181-logical/mod-logical-api-lan.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-wan.odl $(PKGDIR)/etc/amx/tr181-logical/mod-logical-api-wan.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-subnet.odl $(PKGDIR)/etc/amx/tr181-logical/mod-logical-api-subnet.odl
	$(INSTALL) -D -p -m 0644 odl/mod-logical-api-dependencies.odl $(PKGDIR)/etc/amx/tr181-logical/mod-logical-api-dependencies.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/tr181-logical/defaults.gw.d
	$(INSTALL) -D -p -m 0644 odl/defaults.gw.d/*.odl $(PKGDIR)/etc/amx/tr181-logical/defaults.gw.d/
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/mod-logical-api.odl)
	$(eval ODLFILES += odl/mod-logical-api-lan.odl)
	$(eval ODLFILES += odl/mod-logical-api-wan.odl)
	$(eval ODLFILES += odl/mod-logical-api-subnet.odl)
	$(eval ODLFILES += odl/mod-logical-api-dependencies.odl)
ifeq ($(CONFIG_GATEWAY),y)
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/defaults.gw.d/*.odl))
endif

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C mod-force-doracycle/test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test