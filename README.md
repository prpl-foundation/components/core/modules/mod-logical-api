Readme
=======
# mod-logical-api

The api module is used to provide a high level API for configuring interface hierarchies.

## loading/unloading the module
The constructor and destructor functions from this module should be called to make sure that the module works.
