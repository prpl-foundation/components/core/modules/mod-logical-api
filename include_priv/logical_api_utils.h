/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__LOGICAL_API_UTILS_H__)
#define __LOGICAL_API_UTILS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxb/amxb.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_transaction.h>

#define str_empty(txt) ((txt == NULL) || (*txt == 0))
#define CLOSE_QUERY(Q) do { \
        if((Q != NULL) && (*Q != NULL)) { \
            netmodel_closeQuery(*Q); \
            *Q = NULL; \
        } \
} while(0);

amxc_var_t* find_addrs_by_flag(const char* intf, const char* flag);
amxc_var_t* htable_values_to_list(amxc_var_t* src);

amxb_bus_ctx_t* device_get_context(void);
amxb_bus_ctx_t* ra_get_context(void);
amxb_bus_ctx_t* dhcpv4_get_context(void);
amxb_bus_ctx_t* dhcpv6_get_context(void);
amxb_bus_ctx_t* bridging_get_context(void);
amxb_bus_ctx_t* routing_get_context(void);
amxb_bus_ctx_t* ip_get_context(void);

amxc_var_t* find_ip_intfs(const char* intf);
amxc_var_t* find_eth_link_intfs(const char* intf);
amxc_var_t* find_bridge_intfs(const char* intf);
amxc_var_t* find_gpon_intfs(const char* intf);
char* get_eth_link_intf(const char* intf);
char* get_bridge_intf(const char* intf);
char* get_ip_intf(const char* intf);
const char* get_gpon_intf(const char* link_type, amxc_var_t* intf_info);
bool get_status(const char* intf, bool gpon);
char* get_connection_status(const char* intf, bool ppp, bool v4);
char* get_last_conn_error(const char* intf);
char* get_ipv4_router(const char* intf);
char* get_ipv6prefix(const char* intf);

char* get_multi_instance_obj(const char* instance_obj_path);

const char* logical_get_prefix(void);

amxd_status_t trans_apply(amxd_trans_t* trans);
amxd_trans_t* trans_create(const amxd_object_t* const object);

bool iface_has_tag(amxd_object_t* iface_obj, const char* tag);

amxc_string_t* join_strings_csv(const char* str1, const char* str2);
const char* skip_prefix(const char* path);

#ifdef __cplusplus
}
#endif

#endif // __LOGICAL_API_UTILS_H__
