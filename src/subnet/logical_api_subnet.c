/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logical_api_subnet.h"
#include "logical_api_utils.h"
#include "logical_api.h"
#include "logical_api_component.h"

#define ME "subnet-api"

amxd_status_t _CreateSubnet(UNUSED amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            UNUSED amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return amxd_status_function_not_implemented;
}

amxd_status_t _DeleteSubnet(UNUSED amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            UNUSED amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return amxd_status_function_not_implemented;
}

amxd_status_t _GetSubnet(UNUSED amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_object_t* logical_if_obj = NULL;
    const char* alias = GET_CHAR(args, "Alias");
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* eth_link_intf = NULL;
    char* bridge_intf = NULL;

    logical_if_obj = amxd_dm_findf(logical_get_dm(), "Logical.Interface.%s.", alias);
    when_null_trace(logical_if_obj, exit, ERROR, "No logical interface exists for alias %s", alias);
    logical_intf = amxd_object_get_path(logical_if_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_intf, exit, ERROR, "Failed to find logical interface with alias %s", alias);
    ip_intf = get_ip_intf(logical_intf);
    when_str_empty_trace(ip_intf, exit, ERROR, "Failed to find IP interface of logical interface with alias %s", alias);
    eth_link_intf = get_eth_link_intf(logical_intf);
    when_str_empty_trace(eth_link_intf, exit, ERROR, "Failed to find Ethernet link interface of logical interface with alias %s", alias);
    bridge_intf = get_bridge_intf(logical_intf);
    when_str_empty_trace(bridge_intf, exit, ERROR, "Failed to find bridge interface of logical interface with alias %s", alias);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "LogicalInterface", logical_intf);
    amxc_var_add_key(cstring_t, ret, "IPInterface", ip_intf);
    amxc_var_add_key(cstring_t, ret, "EthernetLinkInterface", eth_link_intf);
    amxc_var_add_key(cstring_t, ret, "BridgeInterface", bridge_intf);
    rv = amxd_status_ok;
exit:
    free(logical_intf);
    free(ip_intf);
    free(eth_link_intf);
    free(bridge_intf);
    return rv;
}