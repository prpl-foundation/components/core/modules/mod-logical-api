/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>

#include "logical_api.h"
#include "logical_api_lan.h"
#include "logical_api_subnet.h"
#include "logical_api_wan.h"
#include "logical_api_hostname.h"
#include "logical_api_mutators.h"
#include "logical_api_utils.h"

#ifndef MOD_DIR
#define MOD_DIR "/usr/lib/amx/modules/"
#endif
#define ME "logical-api-main"

static amxo_parser_t* logical_parser = NULL;
static amxd_dm_t* logical_dm = NULL;

amxd_dm_t* logical_get_dm(void) {
    return logical_dm;
}

amxo_parser_t* logical_get_parser(void) {
    return logical_parser;
}

amxb_bus_ctx_t* logical_get_context(void) {
    return amxb_be_who_has("Logical.");
}

/**
 * @brief
 * Helper function to load the needed module on the system.
 *
 * @param module_dir the directory where the module can be found.
 * @param mod_name the name of the module to be loaded.
 * @param so shared file object
 */
static void load_module(const char* module_dir, const char* mod_name, amxm_shared_object_t** so) {
    int retval = -1;
    amxc_string_t mod_path;

    amxc_string_init(&mod_path, 0);

    amxc_string_setf(&mod_path, "%s/%s.so", module_dir, mod_name);
    SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                    mod_name, amxc_string_get(&mod_path, 0));
    retval = amxm_so_open(so, mod_name, amxc_string_get(&mod_path, 0));
    when_failed_trace(retval, exit, WARNING,
                      "Loading controller '%s' failed with error %d", amxc_string_get(&mod_path, 0), retval);
exit:
    amxc_string_clean(&mod_path);
    return;
}


static int logical_api_start(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = -1;
    amxm_shared_object_t* so = NULL;

    when_null_trace(parser, exit, ERROR, "The parser pointer is NULL.");
    when_null_trace(dm, exit, ERROR, "The datamodel pointer is NULL.");
    logical_parser = parser;
    logical_dm = dm;

    netmodel_initialize();

    load_module(MOD_DIR, "mod-force-doracycle", &so);

    rv = 0;

exit:
    return rv;
}

int _logical_api_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case AMXO_START:
        retval = logical_api_start(dm, parser);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to start Logical API module");
        } else {
            SAH_TRACEZ_INFO(ME, "Logical API module started");
        }
        break;
    case AMXO_STOP:
        SAH_TRACEZ_INFO(ME, "Logical API module stopped");
        break;
    }
    return 0;
}

static void wait_depending_objects_cb(UNUSED const char* const signal,
                                      UNUSED const amxc_var_t* const date,
                                      UNUSED void* const priv) {
    amxc_string_t search_path;
    const char* prefix = NULL;
    amxd_object_t* logical_obj = NULL;

    amxc_string_init(&search_path, 0);
    SAH_TRACEZ_INFO(ME, "wait:done received");
    amxp_slot_disconnect(NULL, "wait:done", wait_depending_objects_cb);

    prefix = logical_get_prefix();
    logical_obj = amxd_dm_findf(logical_get_dm(), "%s", "Logical.");
    amxc_string_setf(&search_path, "Interface.*.%sLAN.", prefix); // for some reason, amxd doesn't invoke read action if using [Status=='Enabled']
    amxd_object_for_all(logical_obj, amxc_string_get(&search_path, 0), for_all_init_lan, NULL);

    amxc_string_clean(&search_path);
}

static void wait_for_depending_objects(void) {
    amxd_object_t* logical_obj = amxd_dm_findf(logical_get_dm(), "%s", "Logical.");
    amxc_var_t deps;

    amxc_var_init(&deps);
    amxc_var_convert(&deps, amxd_object_get_param_value(logical_obj, "APIDependencies"), AMXC_VAR_ID_LIST);
    when_true_trace(amxc_var_is_null(&deps), exit, ERROR, "No dependencies found for logical API");

    amxp_slot_connect(NULL, "wait:done", NULL, wait_depending_objects_cb, NULL);
    amxc_var_for_each(dep, &deps) {
        SAH_TRACEZ_INFO(ME, "waiting for dependency %s", GET_CHAR(dep, NULL));
        amxb_wait_for_object(GET_CHAR(dep, NULL));
    }
exit:
    amxc_var_clean(&deps);
    return;
}

void _mod_logical_api_start(UNUSED const char* const sig_name, UNUSED const amxc_var_t* const data, UNUSED void* const priv) {
    amxc_string_t search_path;
    const char* prefix = NULL;
    amxd_object_t* logical_obj = NULL;

    amxc_string_init(&search_path, 0);

    prefix = logical_get_prefix();
    logical_obj = amxd_dm_findf(logical_get_dm(), "%s", "Logical.");
    amxc_string_setf(&search_path, "Interface.*.%sWAN.", prefix); // for some reason, amxd doesn't invoke read action if using [Status=='Enabled']
    amxd_object_for_all(logical_obj, amxc_string_get(&search_path, 0), for_all_init_wan, NULL);

    wait_for_depending_objects();

    amxc_string_clean(&search_path);
}