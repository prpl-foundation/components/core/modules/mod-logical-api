/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logical_api_wan.h"
#include "logical_api.h"
#include "logical_api_utils.h"
#include "logical_api_getters.h"
#include "logical_api_dns.h"

#define ME "wan-api"

typedef enum _protocol {
    UNKNOWN_PROTOCOL,
    DHCP_PROTOCOL,
    PPP_PROTOCOL,
    STATIC_PROTOCOL,
    DSLITE_PROTOCOL,
} protocol_t;

typedef struct _wan_info {
    netmodel_query_t* ipv4_addr_query;
    netmodel_query_t* ipv6_addr_query;
} wan_info_t;

static bool is_wan_enabled(amxd_object_t* iface_obj) {
    return iface_has_tag(iface_obj, "wan");
}

static void wan_info_ctor(amxd_object_t* object) {
    wan_info_t* wan_info = NULL;

    if(object->priv != NULL) {
        SAH_TRACEZ_ERROR(ME, "Trying to allocate an already initialized wan_info_t structure");
        goto exit;
    }

    wan_info = calloc(1, sizeof(wan_info_t));
    when_null_trace(wan_info, exit, ERROR, "Failed to allocate memory for WAN object private data");

    object->priv = wan_info;
exit:
    return;
}

static void wan_priv_free(amxd_object_t* object) {
    wan_info_t* wan_info = NULL;

    when_null(object->priv, exit);

    wan_info = (wan_info_t*) object->priv;

    CLOSE_QUERY(&wan_info->ipv4_addr_query);
    CLOSE_QUERY(&wan_info->ipv6_addr_query);
    free(object->priv);
    object->priv = NULL;
exit:
    return;
}

static void nm_addr_cb(const amxc_var_t* result, void* userdata, bool ipv6) {
    amxc_var_t* addr_var = NULL;
    amxd_object_t* wan_obj = NULL;
    amxd_trans_t* trans = NULL;
    const char* addr_param = ipv6 ? "IPv6Address" : "IPv4Address";

    when_null_trace(userdata, exit, ERROR, "Address callback userdata is NULL");
    wan_obj = (amxd_object_t*) userdata;

    addr_var = GETI_ARG(result, 0);
    trans = trans_create(wan_obj);

    if(addr_var == NULL) {
        amxd_trans_set_value(cstring_t, trans, addr_param, "");
    } else {
        amxd_trans_set_param(trans, addr_param, GET_ARG(addr_var, "Address"));
    }

    trans_apply(trans);
exit:
    return;
}

static void ipv4_addr_cb(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    return nm_addr_cb(result, userdata, false);
}

static void ipv6_addr_cb(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    return nm_addr_cb(result, userdata, true);
}

static int nm_open_addr_queries(amxd_object_t* object) {
    int rv = -1;
    wan_info_t* wan_info = NULL;
    char* logical_if = NULL;

    when_null_trace(object, exit, ERROR, "WAN object is NULL");
    wan_info = (wan_info_t*) object->priv;
    when_null_trace(wan_info, exit, ERROR, "Private WAN info is NULL");

    logical_if = amxd_object_get_path(amxd_object_findf(object, "^"), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_if, exit, ERROR, "Couldn't find logical interface");

    CLOSE_QUERY(&wan_info->ipv4_addr_query);
    CLOSE_QUERY(&wan_info->ipv6_addr_query);

    wan_info->ipv4_addr_query = netmodel_openQuery_getAddrs(logical_if,
                                                            "logical-api", "ipv4 && global", "down", ipv4_addr_cb, object);
    wan_info->ipv6_addr_query = netmodel_openQuery_getAddrs(logical_if,
                                                            "logical-api", "ipv6 && global", "down", ipv6_addr_cb, object);
    when_null_trace(wan_info->ipv4_addr_query, exit, ERROR, "Failed to open IPv4 address query on '%s'", logical_if);
    when_null_trace(wan_info->ipv6_addr_query, exit, ERROR, "Failed to open IPv6 address query on '%s'", logical_if);
    rv = 0;

exit:
    free(logical_if);
    return rv;
}

int for_all_init_wan(UNUSED amxd_object_t* templ, amxd_object_t* wan_obj, UNUSED void* priv) {
    char* status = NULL;
    status = amxd_object_get_value(cstring_t, wan_obj, "Status", NULL);
    if((status != NULL) && (strcmp(status, "Enabled") == 0)) {
        wan_info_ctor(wan_obj);
        when_failed_trace(nm_open_addr_queries(wan_obj), exit, ERROR, "Failed to open query for hostname");
    }

exit:
    free(status);
    return 1;
}

static void WANStatus_fill_link_and_mac_info(amxc_var_t* wan_status, amxc_var_t* intf_info) {
    const char* logical_intf = GET_CHAR(intf_info, "LogicalIntf");
    char* eth_intf = get_eth_link_intf(logical_intf);
    const char* gpon_intf = get_gpon_intf(logical_intf, intf_info);

    // LinkType
    if((eth_intf == NULL) && (gpon_intf != NULL)) {
        amxc_var_add_key(cstring_t, wan_status, "LinkType", "Gpon");
        amxc_var_add_key(cstring_t, wan_status, "LinkState", get_status(gpon_intf, true) ? "Up" : "Down");
        get_device_macaddr(skip_prefix(gpon_intf), wan_status);
    } else if((gpon_intf == NULL) && (eth_intf != NULL)) {
        amxc_var_add_key(cstring_t, wan_status, "LinkType", "Ethernet");
        amxc_var_add_key(cstring_t, wan_status, "LinkState", get_status(eth_intf, false) ? "Up" : "Down");
        get_device_macaddr(skip_prefix(eth_intf), wan_status);
    } else {
        amxc_var_add_key(cstring_t, wan_status, "LinkType", "Unknown");
        amxc_var_add_key(cstring_t, wan_status, "LinkState", "Down");
    }

    free(eth_intf);
}

static protocol_t WANStatus_fill_protocol_info(amxc_var_t* wan_status, amxc_var_t* intf_info) {
    const char* logical_intf = GET_CHAR(intf_info, "LogicalIntf");
    protocol_t protocol_v4 = UNKNOWN_PROTOCOL;
    amxc_var_t* var_intf = netmodel_getIntfs(logical_intf, "ipv4", netmodel_traverse_one_level_down);
    const char* ip_intf = GETI_CHAR(var_intf, 0);

    when_str_empty(ip_intf, exit);
    // protocol v4
    if(netmodel_hasFlag(ip_intf, "dslite", NULL, netmodel_traverse_down)) {
        protocol_v4 = DSLITE_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "Protocol", "DSLite");
    } else if(netmodel_hasFlag(ip_intf, "ppp", NULL, netmodel_traverse_down)) {
        protocol_v4 = PPP_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "Protocol", "PPP");
    } else if(netmodel_hasFlag(ip_intf, "dhcpv4", NULL, netmodel_traverse_down)) {
        protocol_v4 = DHCP_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "Protocol", "DHCP");
    } else if(netmodel_hasFlag(ip_intf, "ipv4-up", NULL, netmodel_traverse_down)) {
        protocol_v4 = STATIC_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "Protocol", "Static");
    } else {
        amxc_var_add_key(cstring_t, wan_status, "Protocol", "None");
    }

exit:
    amxc_var_delete(&var_intf);
    return protocol_v4;
}

static protocol_t WANStatus_fill_IPv6Protocol_info(amxc_var_t* wan_status, amxc_var_t* intf_info) {
    const char* logical_intf = GET_CHAR(intf_info, "LogicalIntf");
    protocol_t protocol_v6 = UNKNOWN_PROTOCOL;
    amxc_var_t* var_intf = netmodel_getIntfs(logical_intf, "ipv6", netmodel_traverse_one_level_down);
    const char* ip_intf = GETI_CHAR(var_intf, 0);

    when_str_empty(ip_intf, exit);
    // protocol v6
    if(netmodel_hasFlag(ip_intf, "ppp", NULL, netmodel_traverse_down)) {
        protocol_v6 = PPP_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "IPv6Protocol", "PPP");
    } else if(netmodel_hasFlag(ip_intf, "dhcpv6", NULL, netmodel_traverse_down)) {
        protocol_v6 = DHCP_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "IPv6Protocol", "DHCP");
    } else if(netmodel_hasFlag(ip_intf, "ipv6-up", NULL, netmodel_traverse_down)) {
        protocol_v6 = STATIC_PROTOCOL;
        amxc_var_add_key(cstring_t, wan_status, "IPv6Protocol", "Static");
    } else {
        amxc_var_add_key(cstring_t, wan_status, "IPv6Protocol", "None");
    }

exit:
    amxc_var_delete(&var_intf);
    return protocol_v6;
}

static void WANStatus_fill_connection_info(amxc_var_t* wan_status, amxc_var_t* intf_info, protocol_t protocol_v4, protocol_t protocol_v6) {
    const char* logical_intf = GET_CHAR(intf_info, "LogicalIntf");

    if(protocol_v4 == DHCP_PROTOCOL) {
        char* connection_state_v4 = get_connection_status(logical_intf, false, true);
        amxc_var_add_key(cstring_t, wan_status, "DHCPv4Status", (const char*) connection_state_v4);
        free(connection_state_v4);
    }

    if((protocol_v4 == PPP_PROTOCOL) || (protocol_v6 == PPP_PROTOCOL)) {
        char* connection_state = get_connection_status(logical_intf, true, false);
        char* connection_error = get_last_conn_error(logical_intf);

        amxc_var_add_key(cstring_t, wan_status, "ConnectionState", (const char*) connection_state);
        amxc_var_add_key(cstring_t, wan_status, "LastConnectionError", (const char*) connection_error);

        free(connection_state);
        free(connection_error);
    }
}

static void WANStatus_fill_address_info(amxc_var_t* wan_status, amxc_var_t* intf_info, protocol_t protocol_v4, protocol_t protocol_v6) {
    const char* log_intf = GET_CHAR(intf_info, "LogicalIntf");
    when_null(log_intf, exit);

    if(protocol_v4 != UNKNOWN_PROTOCOL) {
        amxc_var_t* addrs_params = find_addrs_by_flag(log_intf, "ipv4");
        when_null(addrs_params, exit);
        amxc_var_for_each(ip_block, addrs_params) {
            // IPAddress
            amxc_var_add_key(cstring_t, wan_status, "IPAddress", GET_CHAR(ip_block, "Address"));
            // RemoteGateway
            if(protocol_v4 == PPP_PROTOCOL) {
                amxc_var_add_key(cstring_t, wan_status, "RemoteGateway", GET_CHAR(ip_block, "Peer"));
            } else if(protocol_v4 == DHCP_PROTOCOL) {
                char* remote_gw_dhcp = NULL;
                remote_gw_dhcp = get_ipv4_router(log_intf);
                amxc_var_add_key(cstring_t, wan_status, "RemoteGateway", (const char*) remote_gw_dhcp);
                free(remote_gw_dhcp);
            } else if(protocol_v4 != UNKNOWN_PROTOCOL) {
                amxc_var_t* ip_intf = netmodel_getFirstParameter(log_intf, "InterfacePath", "ipv4", netmodel_traverse_one_level_down);
                get_remote_gateway(GET_CHAR(ip_intf, NULL), wan_status);
                amxc_var_delete(&ip_intf);
            }
        }
        amxc_var_delete(&addrs_params);
    }

    if(protocol_v6 != UNKNOWN_PROTOCOL) {
        amxc_var_t* addrs_params = find_addrs_by_flag(log_intf, "ipv6");
        when_null(addrs_params, exit);
        amxc_var_for_each(ip_block, addrs_params) {
            const char* ipv6_type = GET_CHAR(ip_block, "TypeFlags");
            if((ipv6_type != NULL) && (strncmp(ipv6_type, "@gua", 4) == 0)) {
                // IPv6Address
                amxc_var_add_key(cstring_t, wan_status, "IPv6Address", GET_CHAR(ip_block, "Address"));
            }
        }
        get_remote_v6_gateway(wan_status);
        amxc_var_delete(&addrs_params);
    }

exit:
    return;
}

static void WANStatus_fill_ipv6prefix_info(amxc_var_t* wan_status, amxc_var_t* intf_info, protocol_t protocol_v6) {
    const char* log_intf = GET_CHAR(intf_info, "LogicalIntf");
    char* ipv6prefix_param = NULL;

    when_str_empty(log_intf, exit);
    if(protocol_v6 != UNKNOWN_PROTOCOL) {
        ipv6prefix_param = get_ipv6prefix(log_intf);
        // IPv6DelegatedPrefix
        amxc_var_add_key(cstring_t, wan_status, "IPv6DelegatedPrefix", (const char*) ipv6prefix_param);
    }

exit:
    free(ipv6prefix_param);
}

static void WANStatus_fill_dnsserver_info(amxc_var_t* wan_status, amxc_var_t* intf_info) {
    amxc_var_t* dnsserver_lists = NULL;
    amxc_var_t* ipv4_dns_list = NULL;
    amxc_var_t* ipv6_dns_list = NULL;
    amxc_string_t str;

    amxc_string_init(&str, 0);

    amxc_string_setf(&str, "DNS.Relay.Forwarding.[Interface=='Device.%s' && Status == 'Enabled'].DNSServer", GET_CHAR(intf_info, "LogicalIntf"));

    dnsserver_lists = amxc_var_add_key(amxc_htable_t, wan_status, "DNSServers", NULL);
    ipv4_dns_list = amxc_var_add_key(amxc_llist_t, dnsserver_lists, "v4", NULL);
    ipv6_dns_list = amxc_var_add_key(amxc_llist_t, dnsserver_lists, "v6", NULL);

    get_dns_servers(amxc_string_get(&str, 0), ipv4_dns_list, ipv6_dns_list);

    amxc_string_clean(&str);
}

amxd_status_t _GetWANStatus(amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            UNUSED amxc_var_t* args,
                            amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    amxc_var_t intf_info;
    protocol_t protocol_v4 = UNKNOWN_PROTOCOL;
    protocol_t protocol_v6 = UNKNOWN_PROTOCOL;

    amxc_var_init(&intf_info);
    amxc_var_set_type(&intf_info, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    when_false(is_wan_enabled(amxd_object_get_parent(object)), exit);

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    amxc_var_add_key(cstring_t, &intf_info, "LogicalIntf", logical_intf);
    when_null_trace(GET_CHAR(&intf_info, "LogicalIntf"), exit, ERROR, "Could not find Logical interface");

    WANStatus_fill_link_and_mac_info(ret, &intf_info);
    protocol_v4 = WANStatus_fill_protocol_info(ret, &intf_info);
    protocol_v6 = WANStatus_fill_IPv6Protocol_info(ret, &intf_info);
    WANStatus_fill_connection_info(ret, &intf_info, protocol_v4, protocol_v6);
    WANStatus_fill_address_info(ret, &intf_info, protocol_v4, protocol_v6);
    WANStatus_fill_ipv6prefix_info(ret, &intf_info, protocol_v6);
    WANStatus_fill_dnsserver_info(ret, &intf_info);

    amxc_var_add_key(cstring_t, ret, "Status", netmodel_isUp(logical_intf, "", "down") ? "Up" : "Down");

    rv = amxd_status_ok;
exit:
    amxc_var_clean(&intf_info);
    free(logical_intf);
    return rv;
}

amxd_status_t _read_wan_status(amxd_object_t* object,
                               UNUSED amxd_param_t* param,
                               UNUSED amxd_action_t reason,
                               UNUSED const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               UNUSED void* priv) {
    amxd_object_t* iface_obj = amxd_object_get_parent(object);
    bool enable = is_wan_enabled(iface_obj);
    amxc_var_set(cstring_t, retval, enable ? "Enabled" : "Disabled");
    return amxd_status_ok;
}

amxd_status_t _wan_destroy(amxd_object_t* object,
                           UNUSED amxd_param_t* param,
                           amxd_action_t reason,
                           UNUSED const amxc_var_t* const args,
                           UNUSED amxc_var_t* const retval,
                           UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    wan_priv_free(object);
    status = amxd_status_ok;
exit:
    return status;
}
