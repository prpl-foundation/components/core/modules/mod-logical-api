/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_types.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logical_api_utils.h"
#include "logical_api_component.h"
#include "logical_api_getters.h"

#define ME "getters"

int get_ra_params(const char* intf, amxc_var_t* ret) {
    int rv = -1;
    amxc_string_t search_path;
    amxc_var_t ra_params;

    amxc_string_init(&search_path, 0);
    amxc_string_setf(&search_path, "RouterAdvertisement.InterfaceSetting.[Interface == '%s'].", intf);
    amxc_var_init(&ra_params);

    when_str_empty_trace(intf, exit, ERROR, "Interface is empty, can't get RA parameters");
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    when_failed_trace(component_get_parameters(&ra_params, amxc_string_get(&search_path, 0), ra_get_context()),
                      exit, ERROR, "Failed to fetch RA parameters for interface %s", intf);

    amxc_var_add_key(bool, ret, "RAEnable", GET_BOOL(&ra_params, "Enable"));
    amxc_var_add_key(cstring_t, ret, "RAPool", GET_CHAR(&ra_params, "Alias"));
    rv = 0;
exit:
    amxc_var_clean(&ra_params);
    amxc_string_clean(&search_path);
    return rv;
}

int get_private_ipv4_params(const char* intf, amxc_var_t* ret) {
    int rv = -1;
    when_str_empty(intf, exit);
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");
    rv = get_ip_params(intf, 1, ret);
exit:
    return rv;
}

int get_public_ipv4_params(const char* intf, amxc_var_t* ret) {
    int rv = -1;
    when_str_empty(intf, exit);
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");
    rv = get_ip_params(intf, 2, ret);
exit:
    return rv;
}

int get_ip_params(const char* intf, uint32_t index, amxc_var_t* ret) {
    int rv = -1;
    amxc_string_t search_path;
    amxc_var_t ip_params;

    amxc_string_init(&search_path, 0);
    amxc_string_setf(&search_path, "%sIPv4Address.%u.", intf, index);
    amxc_var_init(&ip_params);

    when_str_empty(intf, exit);
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    when_failed_trace(component_get_parameters(&ip_params, amxc_string_get(&search_path, 0), amxb_be_who_has(intf)),
                      exit, ERROR, "Failed to fetch IP parameters for interface %s", intf);
    amxc_var_add_key(bool, ret, "Enable", GET_BOOL(&ip_params, "Enable"));
    amxc_var_add_key(cstring_t, ret, "IPAddress", GET_CHAR(&ip_params, "IPAddress"));
    amxc_var_add_key(cstring_t, ret, "SubnetMask", GET_CHAR(&ip_params, "SubnetMask"));

    rv = 0;
exit:
    amxc_var_clean(&ip_params);
    amxc_string_clean(&search_path);
    return rv;
}

int get_ipv6_params(const char* intf, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t ip_params;

    amxc_var_init(&ip_params);
    when_str_empty(intf, exit);
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");
    when_failed_trace(component_get_param(&ip_params, intf, amxb_be_who_has(intf), "IPv6Enable"),
                      exit, ERROR, "Failed to fetch IPv6 parameters for interface %s", intf);
    amxc_var_add_key(bool, ret, "Enable", GETP_BOOL(&ip_params, "0.0.IPv6Enable"));

    rv = 0;
exit:
    amxc_var_clean(&ip_params);
    return rv;
}

int get_dhcpv4_params(const char* dhcp_pool, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t dhcp_params;

    amxc_var_init(&dhcp_params);
    when_str_empty_trace(dhcp_pool, exit, ERROR, "DHCP Pool is empty");
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    when_failed_trace(component_get_parameters(&dhcp_params, dhcp_pool, dhcpv4_get_context()),
                      exit, ERROR, "Failed to fetch DHCPv4 parameters for pool %s", dhcp_pool);

    amxc_var_add_key(cstring_t, ret, "Pool", GET_CHAR(&dhcp_params, "Alias"));
    amxc_var_add_key(bool, ret, "Enable", GET_BOOL(&dhcp_params, "Enable"));
    amxc_var_add_key(cstring_t, ret, "MinAddress", GET_CHAR(&dhcp_params, "MinAddress"));
    amxc_var_add_key(cstring_t, ret, "MaxAddress", GET_CHAR(&dhcp_params, "MaxAddress"));
    amxc_var_add_key(uint32_t, ret, "LeaseTime", GET_UINT32(&dhcp_params, "LeaseTime"));
    amxc_var_add_key(cstring_t, ret, "DNSServers", GET_CHAR(&dhcp_params, "DNSServers"));
    amxc_var_add_key(uint32_t, ret, "Order", GET_UINT32(&dhcp_params, "Order"));
    rv = 0;
exit:
    amxc_var_clean(&dhcp_params);
    return rv;
}

int get_dhcpv6_params(const char* dhcp_pool, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t dhcp_params;

    amxc_var_init(&dhcp_params);
    when_str_empty_trace(dhcp_pool, exit, ERROR, "DHCP Pool is empty");
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    when_failed_trace(component_get_parameters(&dhcp_params, dhcp_pool, amxb_be_who_has("DHCPv6Server.")),
                      exit, ERROR, "Failed to fetch DHCPv6 parameters for pool %s", dhcp_pool);

    amxc_var_add_key(cstring_t, ret, "Pool", GET_CHAR(&dhcp_params, "Alias"));
    amxc_var_add_key(bool, ret, "Enable", GET_BOOL(&dhcp_params, "Enable"));
    amxc_var_add_key(bool, ret, "IANAEnable", GET_BOOL(&dhcp_params, "IANAEnable"));
    amxc_var_add_key(bool, ret, "IAPDEnable", GET_BOOL(&dhcp_params, "IAPDEnable"));

    rv = 0;
exit:
    amxc_var_clean(&dhcp_params);
    return rv;
}

int get_device_macaddr(const char* device, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t device_params;

    amxc_var_init(&device_params);
    when_str_empty_trace(device, exit, ERROR, "Device is empty");
    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    when_failed_trace(component_get_parameters(&device_params, device, amxb_be_who_has(device)),
                      exit, ERROR, "Failed to fetch parameters for device %s", device);

    amxc_var_add_key(cstring_t, ret, "MACAddress", GET_CHAR(&device_params, "MACAddress"));

    rv = 0;
exit:
    amxc_var_clean(&device_params);
    return rv;
}

static char* get_dhcp_pool(const char* intf, bool ipv4) {
    char* pool_ref = NULL;
    amxc_string_t query;
    amxc_string_t intf_str;

    amxc_string_init(&intf_str, 0);
    amxc_string_init(&query, 0);

    when_str_empty(intf, exit);

    amxc_string_set(&intf_str, intf);
    if(amxc_string_search(&intf_str, "Device.", 0) == -1) {
        amxc_string_prepend(&intf_str, "Device.", 7);
    }

    amxc_string_setf(&query, "DHCPv%uServer.Pool.[Interface=='%s'].", ipv4 ? 4 : 6, amxc_string_get(&intf_str, 0));

    pool_ref = component_get_path_instance(ipv4 ? dhcpv4_get_context() : amxb_be_who_has("DHCPv6Server."), amxc_string_get(&query, 0));
exit:
    amxc_string_clean(&intf_str);
    amxc_string_clean(&query);
    return pool_ref;
}

char* get_dhcpv4_pool(const char* ip_intf) {
    return get_dhcp_pool(ip_intf, true);
}

char* get_dhcpv6_pool(const char* logical_intf) {
    return get_dhcp_pool(logical_intf, false);
}

char* get_ra_pool(const char* ip_intf) {
    char* pool_ref = NULL;
    amxc_string_t query;

    amxc_string_init(&query, 0);
    when_str_empty(ip_intf, exit);
    amxc_string_setf(&query, "RouterAdvertisement.InterfaceSetting.[Interface=='%s'].", ip_intf);

    pool_ref = component_get_path_instance(ra_get_context(), amxc_string_get(&query, 0));
exit:
    amxc_string_clean(&query);
    return pool_ref;
}

amxc_var_t* collect_ip_params(amxc_var_t* args) {
    amxc_var_t* ret = NULL;
    bool filled = false;
    when_null(args, exit);

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    filled |= (amxc_var_set_key(ret, "Enable", GET_ARG(args, "Enable"), AMXC_VAR_FLAG_COPY) == 0);
    filled |= (amxc_var_set_key(ret, "IPAddress", GET_ARG(args, "IPAddress"), AMXC_VAR_FLAG_COPY) == 0);
    filled |= (amxc_var_set_key(ret, "SubnetMask", GET_ARG(args, "SubnetMask"), AMXC_VAR_FLAG_COPY) == 0);

    if(!filled) {
        amxc_var_delete(&ret);
        ret = NULL;
    }
exit:
    return ret;
}

int collect_ip_params_list(amxc_var_t* args, amxc_var_t** ret) {
    amxc_var_t* ip_configs = GET_ARG(args, "IPConfigs");
    int rv = -1;

    when_null(args, exit);
    when_null(ip_configs, exit);
    when_null(ret, exit);

    amxc_var_new(ret);
    amxc_var_set_type(*ret, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(ip_config, ip_configs) {
        amxc_var_t* data = NULL;
        char* index = amxc_var_dyncast(cstring_t, GET_ARG(ip_config, "Index"));
        when_null_trace(index, exit, ERROR, "IP config must have an index");

        data = amxc_var_add_key(amxc_htable_t, *ret, index, NULL);
        amxc_var_set_key(data, "Enable", GET_ARG(ip_config, "Enable"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(data, "IPAddress", GET_ARG(ip_config, "IPAddress"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(data, "SubnetMask", GET_ARG(ip_config, "SubnetMask"), AMXC_VAR_FLAG_COPY);

        free(index);
    }

    rv = 0;

exit:
    if(rv != 0) {
        amxc_var_delete(ret);
    }
    return rv;
}

amxc_var_t* collect_dhcpv4_params(amxc_var_t* args, const char* ip_intf) {
    amxc_var_t* ret = NULL;
    when_null(args, exit);

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxc_var_set_key(ret, "Enable", GET_ARG(args, "DHCPEnable"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "MinAddress", GET_ARG(args, "MinAddress"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "MaxAddress", GET_ARG(args, "MaxAddress"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "LeaseTime", GET_ARG(args, "LeaseTime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "SubnetMask", GET_ARG(args, "SubnetMask"), AMXC_VAR_FLAG_COPY);
    if(amxc_var_set_key(ret, "DNSServers", GET_ARG(args, "DNSServers"), AMXC_VAR_FLAG_COPY) != 0) {
        amxc_var_set_key(ret, "DNSServers", GET_ARG(args, "IPAddress"), AMXC_VAR_FLAG_COPY);
    }
    amxc_var_set_key(ret, "IPRouters", GET_ARG(args, "IPAddress"), AMXC_VAR_FLAG_COPY);

    if(!str_empty(ip_intf)) {
        amxc_var_add_key(cstring_t, ret, "Interface", ip_intf);
    }
exit:
    return ret;
}

int collect_dhcpv4_pool_params(amxc_var_t* args, const char* ip_intf, amxc_var_t** ret) {
    amxc_var_t* pool_configs = GET_ARG(args, "Pools");
    int rv = -1;

    when_null(args, exit);
    when_null(pool_configs, exit);
    when_null(ret, exit);

    amxc_var_new(ret);
    amxc_var_set_type(*ret, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(pool_config, pool_configs) {
        amxc_var_t* config = NULL;
        amxc_var_t* ip_addr = NULL;
        const char* alias = GET_CHAR(pool_config, "Alias");
        when_str_empty_trace(alias, exit, ERROR, "Pool Alias must be filled in");

        ip_addr = amxp_expr_find_var(args, "IPConfigs.[Index == 1].IPAddress");

        config = amxc_var_add_new_key(*ret, alias);
        amxc_var_set_type(config, AMXC_VAR_ID_HTABLE);
        amxc_var_set_key(config, "Enable", GET_ARG(pool_config, "Enable"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(config, "MinAddress", GET_ARG(pool_config, "MinAddress"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(config, "MaxAddress", GET_ARG(pool_config, "MaxAddress"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(config, "LeaseTime", GET_ARG(pool_config, "LeaseTime"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(config, "SubnetMask", GET_ARG(args, "SubnetMask"), AMXC_VAR_FLAG_COPY);
        amxc_var_set_key(config, "Order", GET_ARG(pool_config, "Order"), AMXC_VAR_FLAG_COPY);
        if(amxc_var_set_key(config, "DNSServers", GET_ARG(pool_config, "DNSServers"), AMXC_VAR_FLAG_COPY) != 0) {
            amxc_var_set_key(config, "DNSServers", ip_addr, AMXC_VAR_FLAG_COPY);
        }
        amxc_var_set_key(config, "IPRouters", ip_addr, AMXC_VAR_FLAG_COPY);

        if(!str_empty(ip_intf)) {
            amxc_var_add_key(cstring_t, config, "Interface", ip_intf);
        }
    }

    rv = 0;

exit:
    if(rv != 0) {
        amxc_var_delete(ret);
    }
    return rv;
}

amxc_var_t* collect_dhcpv6_params(amxc_var_t* args, const char* logical_intf) {
    amxc_var_t* ret = NULL;
    when_null(args, exit);

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxc_var_set_key(ret, "Enable", GET_ARG(args, "DHCPv6Enable"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "IANAEnable", GET_ARG(args, "IANAEnable"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(ret, "IAPDEnable", GET_ARG(args, "IAPDEnable"), AMXC_VAR_FLAG_COPY);
    if(!str_empty(logical_intf)) {
        amxc_var_add_key(cstring_t, ret, "Interface", logical_intf);
    }
exit:
    return ret;
}

amxc_var_t* collect_ra_params(amxc_var_t* args, const char* ip_intf) {
    amxc_var_t* ret = NULL;
    when_null(args, exit);

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxc_var_set_key(ret, "Enable", GET_ARG(args, "RAEnable"), AMXC_VAR_FLAG_COPY);
    if(!str_empty(ip_intf)) {
        amxc_var_add_key(cstring_t, ret, "Interface", ip_intf);
    }
exit:
    return ret;
}

int get_remote_v6_gateway(amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t gateway_data;

    amxc_var_init(&gateway_data);

    when_null_trace(ret, exit, ERROR, "Return variant is NULL");

    rv = amxb_get(routing_get_context(),
                  "Routing.Router.1.IPv6Forwarding.[DestIPPrefix=='::/0' && NextHop != '' && Status == 'Enabled'].",
                  0,
                  &gateway_data,
                  3);

    when_failed_trace(rv, exit, ERROR, "Failed to fetch IPv6RemoteGateway, return '%d'", rv);
    amxc_var_add_key(cstring_t, ret, "IPv6RemoteGateway", GETP_CHAR(&gateway_data, "0.0.NextHop"));

exit:
    amxc_var_clean(&gateway_data);
    return rv;
}
int get_remote_gateway(const char* ip_intf, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t gateway_data;
    amxc_string_t query;

    amxc_var_init(&gateway_data);
    amxc_string_init(&query, 0);

    when_null_trace(ret, exit, ERROR, "Return variant is NULL");
    when_str_empty_trace(ip_intf, exit, ERROR, "IP interface cannot be empty");

    amxc_string_setf(&query,
                     "Routing.Router.1.IPv4Forwarding.[DestIPAddress=='0.0.0.0' && DestSubnetMask == '0.0.0.0' && Status == 'Enabled' && Interface == '%s'].", ip_intf);

    rv = amxb_get(routing_get_context(),
                  amxc_string_get(&query, 0),
                  0,
                  &gateway_data,
                  3);

    when_failed_trace(rv, exit, ERROR, "Failed to fetch RemoteGateway, return '%d'", rv);
    amxc_var_add_key(cstring_t, ret, "RemoteGateway", GETP_CHAR(&gateway_data, "0.0.GatewayIPAddress"));

exit:
    amxc_var_clean(&gateway_data);
    amxc_string_clean(&query);
    return rv;
}
