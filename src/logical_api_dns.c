/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <ipat/ipat.h>

#include "logical_api.h"
#include "logical_api_utils.h"
#include "logical_api_dns.h"
#include "logical_api_component.h"

#define ME "logical"

typedef struct hostname_priv {
    amxd_object_t* hostname_obj;
    netmodel_query_t* nm_addr_query;
    const char* alias;
    bool added;
} hostname_priv_t;

static bool waiting_for_dns = false;

static int for_all_configure_hostname(UNUSED amxd_object_t* templ, amxd_object_t* hostname_obj, UNUSED void* priv) {
    new_hostname(hostname_obj);
    return 1;
}

static int for_all_configure_domain_name(UNUSED amxd_object_t* templ, amxd_object_t* lan_obj, UNUSED void* priv) {
    char* domain_name = NULL;

    if(iface_has_tag(amxd_object_findf(lan_obj, "^"), "lan")) {
        domain_name = amxd_object_get_value(cstring_t, lan_obj, "DomainName", NULL);
        when_str_empty(domain_name, exit);
        dns_set_domain_name(lan_obj, domain_name);
    }
exit:
    free(domain_name);
    return 1;
}

static void wait_done_cb(UNUSED const char* const signal,
                         UNUSED const amxc_var_t* const date,
                         UNUSED void* const priv) {
    amxc_string_t search_path;
    amxd_object_t* obj = NULL;

    amxc_string_init(&search_path, 0);

    SAH_TRACEZ_INFO(ME, "wait:DNS. received");
    amxp_slot_disconnect(NULL, "wait:DNS.", wait_done_cb);
    waiting_for_dns = false;

    amxc_string_setf(&search_path, "Interface.*.%sLAN.HostName.[Enable==true && Name!=''].", logical_get_prefix());
    obj = amxd_dm_findf(logical_get_dm(), "%s", "Logical.");
    amxd_object_for_all(obj, amxc_string_get(&search_path, 0), for_all_configure_hostname, NULL);

    amxc_string_clean(&search_path);
    amxc_string_init(&search_path, 0);

    amxc_string_setf(&search_path, "Interface.[Enable==true].%sLAN.", logical_get_prefix());
    amxd_object_for_all(obj, amxc_string_get(&search_path, 0), for_all_configure_domain_name, NULL);

    amxc_string_clean(&search_path);

}

static amxd_status_t dns_get_context(amxb_bus_ctx_t** bus) {
    amxd_status_t rv = amxd_status_object_not_found;
    SAH_TRACEZ_ERROR(ME, "waiting for DNS");

    *bus = amxb_be_who_has("DNS.");
    if(*bus == NULL) {
        if(!waiting_for_dns) {
            SAH_TRACEZ_INFO(ME, "wait for DNS object");
            amxp_slot_connect_filtered(NULL, "wait:DNS.", NULL, wait_done_cb, NULL);
            amxb_wait_for_object("DNS."); // works async
            waiting_for_dns = true;
        }
        goto exit;
    }

    rv = amxd_status_ok;

exit:
    return rv;
}

static bool dns_object_found(void) {
    amxb_bus_ctx_t* bus = NULL;
    bool found = false;
    found = dns_get_context(&bus) == amxd_status_ok;
    (void) bus;
    return found;
}

static void hostname_priv_t_ctor(amxd_object_t* hostname_obj) {
    hostname_priv_t* hostname_priv = NULL;

    when_null_trace(hostname_obj, exit, ERROR, "Hostname object is NULL");
    if(hostname_obj->priv != NULL) {
        SAH_TRACEZ_INFO(ME, "Hostname object already has private data");
        goto exit;
    }
    hostname_priv = calloc(1, sizeof(hostname_priv_t));
    when_null_trace(hostname_priv, exit, ERROR, "Failed to allocate memory for hostname private data");

    hostname_priv->hostname_obj = hostname_obj;
    hostname_priv->alias = amxd_object_get_name(hostname_obj, AMXD_OBJECT_NAMED);
    hostname_obj->priv = hostname_priv;

exit:
    return;
}

static void cleanup_priv_data(amxd_object_t* hostname_obj) {
    hostname_priv_t* hostname_priv = NULL;

    when_null_trace(hostname_obj, exit, INFO, "Hostname object is NULL");
    hostname_priv = (hostname_priv_t*) hostname_obj->priv;
    when_null_trace(hostname_priv, exit, INFO, "Hostname private data isn't allocated");

    CLOSE_QUERY(&hostname_priv->nm_addr_query);
    free(hostname_priv);
    hostname_obj->priv = NULL;
exit:
    return;
}

static int dns_call_function(const char* method, amxc_var_t* args, amxc_var_t* ret) {
    amxb_bus_ctx_t* bus = NULL;
    amxd_status_t rv = amxd_status_unknown_error;
    rv = dns_get_context(&bus);
    when_failed(rv, exit);
    rv = amxb_call(bus, "DNS.", method, args, ret, 3);
exit:
    return rv;
}

static void dns_remove_hostname(hostname_priv_t* hostname_priv) {
    int rv = -1;
    amxc_var_t ret;
    amxc_var_t params;

    amxc_var_init(&ret);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    when_false(hostname_priv->added, exit);

    when_str_empty_trace(hostname_priv->alias, exit, ERROR, "Failed because alias is empty");
    amxc_var_add_key(cstring_t, &params, "Alias", hostname_priv->alias);

    rv = dns_call_function("RemoveHost", &params, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to remove hostname with alias %s", hostname_priv->alias);

exit:
    hostname_priv->added = false;
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

static int dns_set_hostname(hostname_priv_t* hostname_priv, amxc_var_t* parameters) {
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&ret);
    when_null_trace(parameters, exit, ERROR, "Parameters variant is NULL");

    rv = dns_call_function("SetHost", parameters, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to call DNS function SetHost");
    hostname_priv->added = true;

exit:
    amxc_var_clean(&ret);
    return rv;
}

static void nm_addr_cb(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxc_var_t addr_list;
    amxc_string_t addr_str;
    amxc_var_t params;
    amxd_object_t* hostname_obj = (amxd_object_t*) userdata;
    amxc_var_t* ip_version = NULL;
    char* logical_if = NULL;
    amxc_string_t logical_intf_str;

    amxc_string_init(&logical_intf_str, 0);
    amxc_var_init(&addr_list);
    amxc_var_set_type(&addr_list, AMXC_VAR_ID_LIST);
    amxc_string_init(&addr_str, 0);
    amxc_var_init(&params);

    amxc_var_for_each(var, result) {
        amxc_var_add(cstring_t, &addr_list, GET_CHAR(var, "Address"));
    }
    amxc_string_join_var(&addr_str, &addr_list, ",");

    logical_if = amxd_object_get_path(amxd_object_findf(hostname_obj, "^.^.^"), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_if, exit, ERROR, "Couldn't find logical interface");

    amxc_string_set(&logical_intf_str, logical_if);
    if(amxc_string_search(&logical_intf_str, "Device.", 0) == -1) {
        amxc_string_prepend(&logical_intf_str, "Device.", 7);
    }

    amxd_object_get_params(userdata, &params, amxd_dm_access_protected);
    ip_version = amxc_var_take_key(&params, "IPVersion");
    amxc_var_delete(&ip_version);

    amxc_var_add_key(bool, &params, "Exclusive", true);
    amxc_var_add_key(cstring_t, &params, "Interface", amxc_string_get(&logical_intf_str, 0));
    amxc_var_add_key(cstring_t, &params, "IPAddresses", amxc_string_get(&addr_str, 0));

    when_failed_trace(dns_set_hostname((hostname_priv_t*) hostname_obj->priv, &params), exit, ERROR, "Failed to set hostname");

exit:
    amxc_string_clean(&logical_intf_str);
    amxc_string_clean(&addr_str);
    amxc_var_clean(&addr_list);
    amxc_var_clean(&params);
    free(logical_if);
    return;
}

static int nm_open_addr_query(amxd_object_t* hostname_obj) {
    int rv = -1;
    hostname_priv_t* hostname_priv = NULL;
    char* logical_if = NULL;
    uint32_t ip_version = 0;
    amxc_string_t flag_str;

    amxc_string_init(&flag_str, 0);

    when_null_trace(hostname_obj, exit, ERROR, "Hostname object is NULL");
    hostname_priv = (hostname_priv_t*) hostname_obj->priv;
    when_null_trace(hostname_priv, exit, ERROR, "Private hostname data is NULL");

    logical_if = amxd_object_get_path(amxd_object_findf(hostname_obj, "^.^.^"), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_if, exit, ERROR, "Couldn't find logical interface");

    ip_version = amxd_object_get_value(uint32_t, hostname_obj, "IPVersion", NULL);
    if(ip_version == 0) {
        amxc_string_setf(&flag_str, "%s", "@private || @gua");
    } else if(ip_version == 6) {
        amxc_string_setf(&flag_str, "%s", "@gua");
    } else {
        amxc_string_setf(&flag_str, "%s", "@private");
    }

    CLOSE_QUERY(&hostname_priv->nm_addr_query);
    hostname_priv->nm_addr_query = netmodel_openQuery_getAddrs(logical_if, "lan-mgr", amxc_string_get(&flag_str, 0), "down", nm_addr_cb, hostname_obj);
    when_null_trace(hostname_priv->nm_addr_query, exit, ERROR, "Failed to open address query on '%s'", logical_if);
    rv = 0;

exit:
    amxc_string_clean(&flag_str);
    free(logical_if);
    return rv;
}

bool new_hostname(amxd_object_t* hostname_obj) {
    amxd_status_t rv = amxd_status_unknown_error;
    hostname_priv_t_ctor(hostname_obj);
    when_false(dns_object_found(), exit);
    rv = nm_open_addr_query(hostname_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to open query for hostname");
exit:
    return rv == amxd_status_ok;
}

bool remove_hostname(amxd_object_t* hostname_obj) {
    hostname_priv_t* hostname_priv = (hostname_priv_t*) hostname_obj->priv;
    when_null(hostname_priv, exit);
    if(dns_object_found()) {
        dns_remove_hostname(hostname_priv);
    }
    cleanup_priv_data(hostname_obj);
exit:
    return true;
}

bool set_hostname(amxd_object_t* hostname_obj, const char* name) {

    hostname_priv_t* hostname_priv = (hostname_priv_t*) hostname_obj->priv;
    amxc_var_t params;
    bool rv = true;
    amxc_var_init(&params);
    when_null(hostname_priv, exit);
    when_false(dns_object_found(), exit);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", hostname_priv->alias);
    amxc_var_add_key(cstring_t, &params, "Name", name);
    when_failed_trace(dns_set_hostname(hostname_priv, &params), exit, ERROR, "Failed to change name of hostname %s", hostname_priv->alias);
    rv = true;
exit:
    amxc_var_clean(&params);
    return rv;
}

bool set_hostname_ipversion(amxd_object_t* hostname_obj) {
    bool rv = false;
    hostname_priv_t_ctor(hostname_obj);
    when_false(dns_object_found(), exit);
    when_failed_trace(nm_open_addr_query(hostname_obj), exit, ERROR, "Failed to open query for hostname");
    rv = true;
exit:
    return rv;
}

bool get_dns_servers(const char* search_path, amxc_var_t* ipv4_dns_list, amxc_var_t* ipv6_dns_list) {
    int amxb_rv = -1;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t data;
    amxb_bus_ctx_t* bus = NULL;
    bool rv = false;

    amxc_var_init(&data);

    status = dns_get_context(&bus);
    when_failed_status(status, exit, rv = true); // technically if "DNS."" is not ready then it is ok to return empty list of dns servers?

    amxb_rv = amxb_get(bus, search_path, 0, &data, 5);
    when_failed_trace(amxb_rv, exit, ERROR, "Failed to fetch DNSServers, return '%d'", rv);

    amxc_var_for_each(var, GETI_ARG(&data, 0)) {
        const char* dns_server = GET_CHAR(var, "DNSServer");
        int ipv = ipat_text_family(dns_server);
        if(ipv == AF_INET) {
            amxc_var_add(cstring_t, ipv4_dns_list, dns_server);
        } else if(ipv == AF_INET6) {
            amxc_var_add(cstring_t, ipv6_dns_list, dns_server);
        } else {
            SAH_TRACEZ_ERROR(ME, "DNSServer '%s' is unknown family", dns_server);
        }
    }

    amxc_var_cast(ipv4_dns_list, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(ipv6_dns_list, AMXC_VAR_ID_CSV_STRING);
    rv = true;
exit:
    amxc_var_clean(&data);
    return rv;
}

void dns_set_domain_name(amxd_object_t* lan_obj, const char* domain_name) {
    char* logical_if = NULL;
    amxb_bus_ctx_t* bus = NULL;
    amxd_status_t rv = amxd_status_unknown_error;
    amxc_string_t search_path;

    amxc_string_init(&search_path, 0);

    rv = dns_get_context(&bus);
    when_failed(rv, exit);

    logical_if = amxd_object_get_path(amxd_object_findf(lan_obj, "^"), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_if, exit, ERROR, "Couldn't find logical interface");

    amxc_string_setf(&search_path, "DNS.Relay.Config.[Interface=='Device.%s'].", logical_if);
    component_set_str_param(amxc_string_get(&search_path, 0), bus, "DomainName", domain_name);

exit:
    free(logical_if);
    amxc_string_clean(&search_path);
    return;
}