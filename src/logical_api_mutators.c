/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_types.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <v4v6option.h>

#include "logical_api_utils.h"
#include "logical_api_component.h"
#include "logical_api_getters.h"
#include "logical_api_mutators.h"

#define ME "mutators"

int set_ipv4_parameters(const char* intf, uint32_t addr_idx, amxc_var_t* params) {
    int rv = -1;
    amxc_string_t ip_addr_path;

    amxc_string_init(&ip_addr_path, 0);
    amxc_string_setf(&ip_addr_path, "%sIPv4Address.%u.", intf, addr_idx);

    when_str_empty_trace(intf, exit, ERROR, "IP interface is empty, can't set IPv4 parameters");
    when_null_trace(params, exit, ERROR, "No IP parameters provided to update IPv4 address instance %s", amxc_string_get(&ip_addr_path, 0));

    when_failed_trace(component_set_params(amxc_string_get(&ip_addr_path, 0), amxb_be_who_has(intf), params),
                      exit, ERROR, "Failed to set IPv4Address.%u. on interface %s", addr_idx, intf);
    rv = 0;

exit:
    amxc_string_clean(&ip_addr_path);
    return rv;
}

int set_public_ipv4_params(const char* intf, amxc_var_t* params) {
    return set_ipv4_parameters(intf, 2, params);
}

int set_private_ipv4_params(const char* intf, amxc_var_t* params) {
    return set_ipv4_parameters(intf, 1, params);
}

char* create_bridge_port(const char* bridge_intf, const char* lower_layer) {
    char* bridge_port_multi_inst = get_multi_instance_obj(bridge_intf);
    char* created_bridge_port = NULL;
    amxc_var_t values;
    amxc_var_init(&values);

    when_str_empty_trace(bridge_port_multi_inst, exit, ERROR, "Failed to get multi instance object from %s", bridge_intf);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "LowerLayers", lower_layer);
    amxc_var_add_key(bool, &values, "Enable", true);

    created_bridge_port = component_add_instance(bridge_port_multi_inst, NULL, &values, bridging_get_context());
    if(str_empty(created_bridge_port)) {
        SAH_TRACEZ_ERROR(ME, "Failed to created bridge port for LowerLayerInterface %s", lower_layer);
    }

exit:
    free(bridge_port_multi_inst);
    amxc_var_clean(&values);
    return created_bridge_port;
}

static char* update_pool_params(const char* curr_pool, const char* new_pool_alias, amxc_var_t* params, amxb_bus_ctx_t* ctx, const char* multi_inst_obj_path) {
    int rv = -1;
    char* new_pool_path = NULL;

    when_str_empty_trace(multi_inst_obj_path, exit, ERROR, "Multi instance object path is empty");
    if(str_empty(curr_pool) && str_empty(new_pool_alias)) {
        SAH_TRACEZ_ERROR(ME, "Can't update pool parameters, current pool and new pool alias are both empty");
    } else if(!str_empty(curr_pool) && str_empty(new_pool_alias)) {
        rv = component_set_params(curr_pool, ctx, params);
        if(rv == 0) {
            new_pool_path = strdup(curr_pool);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to set parameters for pool %s", curr_pool);
            goto exit;
        }
    } else if(!str_empty(new_pool_alias)) {
        amxc_string_t new_pool;
        amxc_string_init(&new_pool, 0);

        amxc_string_setf(&new_pool, "%s%s.", multi_inst_obj_path, new_pool_alias);
        new_pool_path = component_get_path_instance(ctx, amxc_string_get(&new_pool, 0));

        if(!str_empty(curr_pool)) {
            rv = component_set_str_param(curr_pool, ctx, "Interface", "");
            when_failed_trace(rv, cleanup, ERROR, "Failed to clear Interface parameter from current pool %s", curr_pool);
        }

        if(str_empty(new_pool_path)) {
            free(new_pool_path);
            // New pool doesn't exist, create it
            new_pool_path = component_add_instance(multi_inst_obj_path, new_pool_alias, params, ctx);
            if(str_empty(new_pool_path)) {
                SAH_TRACEZ_ERROR(ME, "Failed to create new pool instance with alias %s", new_pool_alias);
                goto cleanup;
            }
        } else {
            rv = component_set_params(new_pool_path, ctx, params);
            if(rv != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to set parameters for pool %s", new_pool_path);
                free(new_pool_path);
                new_pool_path = NULL;
            }
        }
cleanup:
        amxc_string_clean(&new_pool);
    }
exit:
    return new_pool_path;
}

char* set_dhcpv4_params(const char* curr_pool, const char* new_pool_alias, amxc_var_t* params) {
    return update_pool_params(curr_pool, new_pool_alias, params, dhcpv4_get_context(), "DHCPv4Server.Pool.");
}

char* set_dhcpv6_params(const char* curr_pool, const char* new_pool_alias, amxc_var_t* params) {
    return update_pool_params(curr_pool, new_pool_alias, params, dhcpv4_get_context(), "DHCPv6Server.Pool.");
}

char* set_ra_params(const char* curr_pool, const char* new_pool_alias, amxc_var_t* params) {
    return update_pool_params(curr_pool, new_pool_alias, params, ra_get_context(), "RouterAdvertisement.InterfaceSetting.");
}

int set_dhcp_option(const char* pool, uint32_t option, const char* val, uint8_t ip_version) {
    int rv = -1;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t params;
    char* option_path = NULL;
    amxc_string_t search_path;

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&search_path, 0);

    if((ip_version != 4) && (ip_version != 6)) {
        SAH_TRACEZ_ERROR(ME, "Incorrect IP version: %u", ip_version);
        goto exit;
    }

    when_str_empty_trace(pool, exit, ERROR, "DHCPv%u pool reference is empty", ip_version);

    ctx = ip_version == 4 ? dhcpv4_get_context() : amxb_be_who_has("DHCPv6Server.");
    when_null_trace(ctx, exit, ERROR, "Couldn't find DHCPv%u bus context", ip_version);

    amxc_string_setf(&search_path, "%sOption.[Tag == %u].", pool, option);
    option_path = component_get_path_instance(ctx, amxc_string_get(&search_path, 0));

    amxc_var_add_key(uint32_t, &params, "Tag", option);

    // Convert value string to hexbinary
    if(str_empty(val)) {
        amxc_var_add_key(bool, &params, "Enable", false);
        amxc_var_add_key(cstring_t, &params, "Value", "");
    } else {
        uint32_t len = 0;
        const char* value = NULL;

        if(ip_version == 4) {
            value = (const char*) dhcpoption_v4_str2bin(option, val, &len);
            amxc_var_add_key(cstring_t, &params, "Value", value);
        } else {
            amxc_var_t src;
            amxc_var_t dest;

            amxc_var_init(&src);
            amxc_var_init(&dest);

            amxc_var_set(cstring_t, &src, val);
            dhcpoption_v6_option_to_hex(option, &dest, &src);
            value = GET_CHAR(&dest, NULL);
            amxc_var_add_key(cstring_t, &params, "Value", value);

            amxc_var_clean(&src);
            amxc_var_clean(&dest);
        }
        amxc_var_add_key(bool, &params, "Enable", true);
    }

    // Either update the existing option or create a new option instance
    if(str_empty(option_path)) {
        amxc_string_t alias;
        amxc_string_init(&alias, 0);
        char* path = NULL;

        amxc_string_setf(&alias, "option-%u", option);
        amxc_string_setf(&search_path, "%sOption.", pool);

        path = component_add_instance(amxc_string_get(&search_path, 0), amxc_string_get(&alias, 0), &params, ctx);
        amxc_string_clean(&alias);
        free(path);
    } else {
        component_set_params(option_path, ctx, &params);
    }

    rv = 0;
exit:
    free(option_path);
    amxc_var_clean(&params);
    amxc_string_clean(&search_path);
    return rv;
}

void set_ipv6_dsl(amxd_object_t* object) {
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* dhcpv6_pool = NULL;
    char* ra_pool = NULL;
    char* dsl = NULL;
    char* domain_name = NULL;
    amxc_string_t* combined_domains = NULL;

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_intf, exit, ERROR, "Couldn't find logical interface");

    ip_intf = get_ip_intf(logical_intf);

    dsl = amxd_object_get_value(cstring_t, object, "DomainSearchList", NULL);
    domain_name = amxd_object_get_value(cstring_t, object, "DomainName", NULL);
    combined_domains = join_strings_csv(dsl, domain_name);

    ra_pool = get_ra_pool(ip_intf);
    if(!str_empty(ra_pool)) {
        if(component_set_str_param(ra_pool, ra_get_context(), "DNSSL", amxc_string_get(combined_domains, 0)) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to set the DomainSearchList option for RA");
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "Interface %s does not have an associated RA pool", logical_intf);
    }

    dhcpv6_pool = get_dhcpv6_pool(logical_intf);
    if(!str_empty(dhcpv6_pool)) {
        if(set_dhcp_option(dhcpv6_pool, 24, amxc_string_get(combined_domains, 0), 6) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to set option 24 in DHCPv6 pool %s for interface %s", dhcpv6_pool, logical_intf);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "Interface %s does not have an associated DHCPv6 pool", logical_intf);
    }

exit:
    free(logical_intf);
    free(ip_intf);
    free(ra_pool);
    free(dhcpv6_pool);
    free(domain_name);
    free(dsl);
    amxc_string_delete(&combined_domains);
    return;
}

void set_ipv4_dsl(amxd_object_t* object) {
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* dhcpv4_pool = NULL;
    char* dsl = NULL;

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_intf, exit, ERROR, "Couldn't find logical interface");

    ip_intf = get_ip_intf(logical_intf);
    dhcpv4_pool = get_dhcpv4_pool(ip_intf);
    dsl = amxd_object_get_value(cstring_t, object, "DomainSearchList", NULL);

    if(!str_empty(dhcpv4_pool)) {
        if(set_dhcp_option(dhcpv4_pool, 119, dsl, 4) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to set option 119 in DHCPv4 pool %s", dhcpv4_pool);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "Interface %s does not have an associated DHCPv4 pool", logical_intf);
    }

exit:
    free(ip_intf);
    free(logical_intf);
    free(dhcpv4_pool);
    free(dsl);
    return;
}

void set_domain_name(amxd_object_t* object) {
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* dhcpv4_pool = NULL;
    char* domain_name = NULL;

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_intf, exit, ERROR, "Couldn't find logical interface");

    ip_intf = get_ip_intf(logical_intf);
    dhcpv4_pool = get_dhcpv4_pool(ip_intf);
    domain_name = amxd_object_get_value(cstring_t, object, "DomainName", NULL);

    if(!str_empty(dhcpv4_pool)) {
        if(component_set_str_param(dhcpv4_pool, dhcpv4_get_context(), "DomainName", domain_name) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to set domain name in DHCPv4 pool %s", dhcpv4_pool);
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Interface %s does not have an associated DHCPv4 pool", logical_intf);
    }

exit:
    free(ip_intf);
    free(logical_intf);
    free(dhcpv4_pool);
    free(domain_name);
    return;
}
