/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <netmodel/common_api.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logical_api_utils.h"
#include "logical_api_component.h"
#include "logical_api.h"
#include "logical_api_lan.h"

#define ME "api-utils"

static amxc_var_t* find_interface_paths_by_flag(const char* intf, const char* flag) {
    amxc_var_t* nm_ret = NULL;
    amxc_var_t* ret = NULL;

    nm_ret = netmodel_getParameters(intf, "InterfacePath", flag, "down");
    when_null(nm_ret, exit);
    ret = htable_values_to_list(nm_ret);

exit:
    amxc_var_delete(&nm_ret);
    return ret;
}

static amxc_var_t* find_parameters_by_flag(const char* intf, const char* param_name, const char* flag) {
    amxc_var_t* nm_ret = NULL;
    amxc_var_t* ret = NULL;

    nm_ret = netmodel_getFirstParameter(intf, param_name, flag, "down");
    when_null(nm_ret, exit);

    ret = htable_values_to_list(nm_ret);

exit:
    amxc_var_delete(&nm_ret);
    return ret;
}

amxc_var_t* find_addrs_by_flag(const char* intf, const char* flag) {
    amxc_var_t* nm_ret = NULL;
    amxc_var_t* ret = NULL;

    nm_ret = netmodel_getAddrs(intf, flag, "down");
    when_null(nm_ret, exit);

    ret = htable_values_to_list(nm_ret);

exit:
    amxc_var_delete(&nm_ret);
    return ret;
}

amxc_var_t* htable_values_to_list(amxc_var_t* src) {
    amxc_var_t* list = NULL;
    amxc_var_new(&list);

    amxc_var_convert(list, src, AMXC_VAR_ID_LIST);
    return list;
}

amxc_var_t* find_ip_intfs(const char* intf) {
    return find_interface_paths_by_flag(intf, "ip");
}

char* get_ip_intf(const char* intf) {
    amxd_object_t* intf_obj = NULL;
    char* ip_intf = NULL;
    amxc_var_t* ip_intf_list = NULL;

    intf_obj = amxd_dm_findf(logical_get_dm(), "%s", intf);
    when_null_trace(intf_obj, exit, ERROR, "Could not find interface object matching %s", intf);

    if(iface_has_tag(intf_obj, "lan")) {
        amxd_object_t* lan_obj = amxd_object_findf(intf_obj, "%sLAN.", logical_get_prefix());
        when_null_trace(lan_obj, exit, ERROR, "Could not find LAN object matching %s", intf);
        ip_intf = lan_get_ip_intf(lan_obj);
    } else {
        ip_intf_list = find_ip_intfs(intf);
        when_null(GETI_CHAR(ip_intf_list, 0), exit);
        ip_intf = strdup(GETI_CHAR(ip_intf_list, 0));
    }
exit:
    amxc_var_delete(&ip_intf_list);
    return ip_intf;
}

char* get_eth_link_intf(const char* intf) {
    amxc_var_t* eth_link_intf_list = find_eth_link_intfs(intf);
    char* eth_link_intf = NULL;

    when_null(GETI_CHAR(eth_link_intf_list, 0), exit);
    eth_link_intf = strdup(GETI_CHAR(eth_link_intf_list, 0));
exit:
    amxc_var_delete(&eth_link_intf_list);
    return eth_link_intf;
}

char* get_bridge_intf(const char* intf) {
    amxc_var_t* bridge_intf_list = find_bridge_intfs(intf);
    char* bridge_intf = NULL;

    when_null(GETI_CHAR(bridge_intf_list, 0), exit);
    bridge_intf = strdup(GETI_CHAR(bridge_intf_list, 0));
exit:
    amxc_var_delete(&bridge_intf_list);
    return bridge_intf;
}

const char* get_gpon_intf(const char* intf, amxc_var_t* intf_info) {
    amxc_var_t* gpon_intf_list = find_gpon_intfs(intf);

    amxc_var_add_key(cstring_t, intf_info, "GPONIntf", GETI_CHAR(gpon_intf_list, 0));
    amxc_var_delete(&gpon_intf_list);
    return GET_CHAR(intf_info, "GPONIntf");
}

bool get_status(const char* intf, bool gpon) {
    amxc_var_t* status = NULL;
    bool status_bool = false;

    if(gpon) {
        status = find_parameters_by_flag(intf, "Status", "gpon");
    } else {
        status = find_parameters_by_flag(intf, "Status", "eth_link");
    }

    status_bool = GETI_BOOL(status, 0);

    amxc_var_delete(&status);
    return status_bool;
}

char* get_connection_status(const char* intf, bool ppp, bool v4) {
    amxc_var_t* connection_intf_list = NULL;
    char* connection_intf = NULL;

    if(ppp) {
        connection_intf_list = find_parameters_by_flag(intf, "ConnectionStatus", "ppp-up");
    } else {
        connection_intf_list = find_parameters_by_flag(intf, "DHCPStatus", v4 ? "dhcpv4 && up" : "dhcpv6 && up");
    }

    when_null(GETI_CHAR(connection_intf_list, 0), exit);
    connection_intf = strdup(GETI_CHAR(connection_intf_list, 0));
exit:
    amxc_var_delete(&connection_intf_list);
    return connection_intf;
}

char* get_last_conn_error(const char* intf) {
    amxc_var_t* connection_error = find_parameters_by_flag(intf, "LastConnectionError", "ppp-up");
    char* connection_intf = NULL;

    when_null(GETI_CHAR(connection_error, 0), exit);
    connection_intf = strdup(GETI_CHAR(connection_error, 0));
exit:
    amxc_var_delete(&connection_error);
    return connection_intf;
}

char* get_ipv4_router(const char* intf) {
    amxc_var_t* ip_router = find_parameters_by_flag(intf, "IPRouters", "ipv4-up");
    char* remote_gateway = NULL;

    when_null(GETI_CHAR(ip_router, 0), exit);
    remote_gateway = strdup(GETI_CHAR(ip_router, 0));
exit:
    amxc_var_delete(&ip_router);
    return remote_gateway;
}

char* get_ipv6prefix(const char* intf) {
    amxc_var_t* ipv6_prefix = netmodel_getFirstIPv6Prefix(intf, "PrefixDelegation", "down");
    char* prefix = NULL;

    when_null(GET_CHAR(ipv6_prefix, "Prefix"), exit);
    prefix = strdup(GET_CHAR(ipv6_prefix, "Prefix"));
exit:
    amxc_var_delete(&ipv6_prefix);
    return prefix;
}

amxc_var_t* find_eth_link_intfs(const char* intf) {
    return find_interface_paths_by_flag(intf, "eth_link");
}

amxc_var_t* find_bridge_intfs(const char* intf) {
    return find_interface_paths_by_flag(intf, "bridge");
}

amxc_var_t* find_gpon_intfs(const char* intf) {
    return find_interface_paths_by_flag(intf, "gpon");
}

amxb_bus_ctx_t* ra_get_context(void) {
    return amxb_be_who_has("RouterAdvertisement.");
}

amxb_bus_ctx_t* dhcpv4_get_context(void) {
    return amxb_be_who_has("DHCPv4Server.");
}

amxb_bus_ctx_t* dhcpv6_get_context(void) {
    return amxb_be_who_has("DHCPv6Server.");
}

amxb_bus_ctx_t* device_get_context(void) {
    return amxb_be_who_has("Device.");
}

amxb_bus_ctx_t* bridging_get_context(void) {
    return amxb_be_who_has("Bridging.");
}

amxb_bus_ctx_t* routing_get_context(void) {
    return amxb_be_who_has("Routing.");
}

amxb_bus_ctx_t* ip_get_context(void) {
    return amxb_be_who_has("IP.");
}

char* get_multi_instance_obj(const char* instance_obj_path) {
    amxd_path_t multi_instance_obj_path;
    char* return_path = NULL;
    when_failed_trace(amxd_path_init(&multi_instance_obj_path, instance_obj_path),
                      exit, ERROR, "Failed to initialize path structure for object %s", instance_obj_path);
    free(amxd_path_get_last(&multi_instance_obj_path, true));
    return_path = strdup(amxd_path_get(&multi_instance_obj_path, AMXD_OBJECT_TERMINATE));

exit:
    amxd_path_clean(&multi_instance_obj_path);
    return return_path;
}

const char* logical_get_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(logical_get_parser(), "prefix_");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return prefix == NULL ? "" : prefix;
}

amxd_status_t trans_apply(amxd_trans_t* trans) {
    amxd_status_t status = amxd_status_unknown_error;
    when_null(trans, exit);

    status = amxd_trans_apply(trans, logical_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", status);
    }
    amxd_trans_delete(&trans);

exit:
    return status;
}

amxd_trans_t* trans_create(const amxd_object_t* const object) {
    amxd_trans_t* trans = NULL;

    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, object);

exit:
    return trans;
}

bool iface_has_tag(amxd_object_t* iface_obj, const char* tag) {
    const char* tags = GET_CHAR(amxd_object_get_param_value(iface_obj, "Tags"), NULL);
    return strstr(tags, tag) != NULL;
}

amxc_string_t* join_strings_csv(const char* str1, const char* str2) {
    amxc_string_t* result = NULL;

    amxc_string_new(&result, 0);

    if(!str_empty(str1)) {
        amxc_string_set(result, str1);
        if(!str_empty(str2)) {
            amxc_string_appendf(result, ",%s", str2);
        }
    } else {
        if(!str_empty(str2)) {
            amxc_string_set(result, str2);
        } else {
            amxc_string_set(result, "");
        }
    }
    return result;
}

const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";
    size_t len = strlen(prefix);

    when_str_empty(path, exit);

    if(strncmp(path, prefix, len) == 0) {
        path += len;
    }

exit:
    return path;
}
