/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#include "logical_api_hostname.h"
#include "logical_api.h"
#include "logical_api_utils.h"
#include "logical_api_dns.h"

#define ME "hostname"

void _hostname_added(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(logical_get_dm(), data);
    amxd_object_t* hostname_obj = NULL;
    bool enable = GETP_BOOL(data, "parameters.Enable");

    when_null_trace(obj, exit, ERROR, "Data model object from signal %s is NULL", sig_name);
    hostname_obj = amxd_object_get_instance(obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(hostname_obj, exit, ERROR, "Hostname object is NULL");

    if(enable) {
        when_false(new_hostname(hostname_obj), exit);
    }
exit:
    return;
}

amxd_status_t _hostname_removed(amxd_object_t* object,
                                UNUSED amxd_param_t* param,
                                amxd_action_t reason,
                                UNUSED const amxc_var_t* const args,
                                UNUSED amxc_var_t* const retval,
                                UNUSED void* priv) {

    amxd_status_t status = amxd_status_unknown_error;

    when_false_status(reason == action_object_destroy, exit, status = amxd_status_function_not_implemented);
    when_false(remove_hostname(object), exit);
    status = amxd_status_ok;
exit:
    return status;
}

void _toggle_hostname(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    amxd_object_t* hostname_obj = amxd_dm_signal_get_object(logical_get_dm(), data);
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    if(enable) {
        new_hostname(hostname_obj);
    } else {
        remove_hostname(hostname_obj);
    }
}

void _name_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* hostname_obj = amxd_dm_signal_get_object(logical_get_dm(), data);
    bool enabled = amxd_object_get_value(bool, hostname_obj, "Enable", NULL);

    if(enabled) {
        set_hostname(hostname_obj, GETP_CHAR(data, "parameters.Name.to"));
    }
}

void _ip_version_changed(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    amxd_object_t* hostname_obj = amxd_dm_signal_get_object(logical_get_dm(), data);
    bool enabled = amxd_object_get_value(bool, hostname_obj, "Enable", NULL);

    if(enabled) {
        set_hostname_ipversion(hostname_obj);
    }
}
