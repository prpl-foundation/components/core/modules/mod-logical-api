/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <netmodel/common_api.h>

#include "logical_api_lan.h"
#include "logical_api.h"
#include "logical_api_utils.h"
#include "logical_api_component.h"
#include "logical_api_validate.h"
#include "logical_api_getters.h"
#include "logical_api_mutators.h"
#include "logical_api_dns.h"

#define ME "lan-api"

typedef struct _lan_info {
    netmodel_query_t* ip_intf_query;
    char* ip_intf;
} lan_info_t;

static int is_dot(int c) {
    return (c == '.') ? 1 : 0;
}

/**
 * @brief Function to trim the trailing dot from a char string if there is one
 * @param path char string containing the path that should have the dot removed
 * @return returns pointer to a string buffer containing the path string without a trailing dot
 * @note This function allocates memory to store the trimmed path. The memory needs to be freed using "free" if not needed anymore
 */
static char* trim_final_dot(const char* path) {
    amxc_string_t trimmed_path;
    amxc_string_init(&trimmed_path, 0);

    when_str_empty_trace(path, exit, ERROR, "No path given to trim");

    amxc_string_set(&trimmed_path, path);
    amxc_string_trimr(&trimmed_path, is_dot);
exit:
    return amxc_string_take_buffer(&trimmed_path);
}

static void lan_info_ctor(amxd_object_t* object) {
    lan_info_t* lan_info = NULL;

    if(object->priv != NULL) {
        SAH_TRACEZ_ERROR(ME, "Trying to allocate an already initialized lan_info_t structure");
        goto exit;
    }

    lan_info = calloc(1, sizeof(lan_info_t));
    when_null_trace(lan_info, exit, ERROR, "Failed to allocate memory for LAN object private data");

    object->priv = lan_info;
exit:
    return;
}

static void lan_priv_free(amxd_object_t* object) {
    lan_info_t* lan_info = NULL;

    when_null(object->priv, exit);

    lan_info = (lan_info_t*) object->priv;

    CLOSE_QUERY(&lan_info->ip_intf_query);
    free(lan_info->ip_intf);
    free(object->priv);
    object->priv = NULL;
exit:
    return;
}

static void nm_ip_intf_cb(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    const char* intf = NULL;
    amxd_object_t* lan_obj = NULL;

    when_null_trace(userdata, exit, ERROR, "Address callback userdata is NULL");
    lan_obj = (amxd_object_t*) userdata;
    when_null_trace(lan_obj, exit, ERROR, "LAN object is NULL");

    intf = GET_CHAR(result, NULL);

    if(!str_empty(intf)) {
        lan_info_t* lan_info = lan_obj->priv;
        when_null_trace(lan_info, exit, ERROR, "LAN private data is NULL");

        lan_info->ip_intf = strdup(intf);
        set_ipv4_dsl(lan_obj);
        set_ipv6_dsl(lan_obj);
        set_domain_name(lan_obj);
    }

exit:
    return;
}

static int nm_open_intf_query(amxd_object_t* object) {
    int rv = -1;
    lan_info_t* lan_info = NULL;
    char* logical_if = NULL;

    when_null_trace(object, exit, ERROR, "LAN object is NULL");
    lan_info = (lan_info_t*) object->priv;
    when_null_trace(lan_info, exit, ERROR, "Private LAN data is NULL");

    logical_if = amxd_object_get_path(amxd_object_findf(object, "^"), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(logical_if, exit, ERROR, "Couldn't find logical interface");

    CLOSE_QUERY(&lan_info->ip_intf_query);

    lan_info->ip_intf_query = netmodel_openQuery_getFirstParameter(logical_if,
                                                                   "logical-api", "InterfacePath", "ip", netmodel_traverse_down, nm_ip_intf_cb, object);

    when_null_trace(lan_info->ip_intf_query, exit, ERROR, "Failed to open IP interface query on '%s'", logical_if);
    rv = 0;

exit:
    free(logical_if);
    return rv;
}

static bool is_lan_enabled(amxd_object_t* iface_obj) {
    return iface_has_tag(iface_obj, "lan");
}

static int force_dora_cycle(const char* intf) {
    int rv = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    when_str_empty_trace(intf, exit, ERROR, "No interface on which to force the DORA cycle provided");
    SAH_TRACEZ_INFO(ME, "Forcing DORA cycle on '%s'", intf);
    amxc_var_add_key(cstring_t, &data, "Interface", intf);
    rv = amxm_execute_function(MOD_FORCE, MOD_FORCE, "force_lowermost_layer_toggle", &data, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to force dora cycle on '%s'", intf);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return rv;
}

int for_all_init_lan(UNUSED amxd_object_t* templ, amxd_object_t* lan_obj, UNUSED void* priv) {
    char* status = NULL;
    status = amxd_object_get_value(cstring_t, lan_obj, "Status", NULL);
    if((status != NULL) && (strcmp(status, "Enabled") == 0)) {
        lan_info_ctor(lan_obj);
        when_failed_trace(nm_open_intf_query(lan_obj), exit, ERROR, "Failed to open query for IP interface");
    }

exit:
    free(status);
    return 1;
}

char* lan_get_ip_intf(amxd_object_t* lan_obj) {
    char* ip_intf = NULL;
    lan_info_t* lan_info = NULL;
    when_null(lan_obj, exit);

    lan_info = lan_obj->priv;
    when_null(lan_info, exit);

    if(lan_info->ip_intf != NULL) {
        ip_intf = strdup(lan_info->ip_intf);
    }
exit:
    return ip_intf;
}

amxd_status_t _SetIPv4Config(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    amxc_var_t* ip_params = NULL;
    amxc_var_t* dhcpv4_params = NULL;

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);

    when_str_empty_trace(ip_intf, exit, ERROR, "Failed to get IP interface");
    ip_params = collect_ip_params(args);
    dhcpv4_params = collect_dhcpv4_params(args, ip_intf);

    if(ip_params != NULL) {
        // validate_ip_params
        if(is_valid_ip_network(ip_params, skip_prefix(ip_intf)) == 0) {
            rv = amxd_status_invalid_function_argument;
            SAH_TRACEZ_ERROR(ME, "IP parameters are invalid");
            goto exit;
        }
        when_failed_trace(set_private_ipv4_params(skip_prefix(ip_intf), ip_params),
                          exit, ERROR, "Failed to set private IPv4 parameters for interface %s", ip_intf);
    }

    if(dhcpv4_params != NULL) {
        const char* new_pool_alias = GET_CHAR(args, "Pool");
        char* curr_dhcpv4_pool = amxd_object_get_value(cstring_t, object, "DHCPv4PoolReference", NULL);
        char* new_dhcpv4_pool = NULL;

        if(str_empty(curr_dhcpv4_pool)) {
            free(curr_dhcpv4_pool);
            curr_dhcpv4_pool = get_dhcpv4_pool(ip_intf);
        }
        new_dhcpv4_pool = set_dhcpv4_params(curr_dhcpv4_pool, new_pool_alias, dhcpv4_params);

        free(curr_dhcpv4_pool);
        if(str_empty(new_dhcpv4_pool)) {
            SAH_TRACEZ_ERROR(ME, "Failed to update DHCPv4 params");
            free(new_dhcpv4_pool);
            goto exit;
        } else {
            amxd_object_set_value(cstring_t, object, "DHCPv4PoolReference", new_dhcpv4_pool);
            free(new_dhcpv4_pool);
            if(GET_BOOL(args, "ForceDoraCycle")) {
                force_dora_cycle(ip_intf);
            }
        }
    }
    rv = amxd_status_ok;

exit:
    free(ip_intf);
    free(logical_intf);
    amxc_var_delete(&ip_params);
    amxc_var_delete(&dhcpv4_params);
    return rv;
}

amxd_status_t _SetIPv4ConfigExt(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    char* ip_intf = get_ip_intf(logical_intf);
    amxc_var_t* ip_params = NULL;
    amxc_var_t* dhcpv4_params = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty_trace(ip_intf, exit, ERROR, "Failed to get IP interface");

    rv = amxd_status_invalid_function_argument;
    when_failed_trace(collect_ip_params_list(args, &ip_params), exit, ERROR,
                      "Invalid IP configuration given");
    when_failed_trace(collect_dhcpv4_pool_params(args, ip_intf, &dhcpv4_params), exit, ERROR,
                      "Invalid DHCP Pool configuration given");

    amxc_var_for_each(ip_config, ip_params) {
        uint32_t index = strtoul(amxc_var_key(ip_config), NULL, 0);

        // validate_ip_params and collecting wrong ones
        if(is_valid_ip_network(ip_config, skip_prefix(ip_intf)) == 0) {

            rv = amxd_status_invalid_function_argument;
            SAH_TRACEZ_ERROR(ME, "IP parameters are invalid");
            goto exit;
        }

        when_failed_trace(set_ipv4_parameters(skip_prefix(ip_intf), index, ip_config), exit, ERROR,
                          "Failed to update IP config for IP address %d", index);
    }

    amxc_var_for_each(pool_config, dhcpv4_params) {
        const char* alias = amxc_var_key(pool_config);
        char* pool_path = set_dhcpv4_params(NULL, alias, pool_config);

        if(str_empty(pool_path)) {
            SAH_TRACEZ_ERROR(ME, "Failed to update DHCPv4 pool %s", alias);
            free(pool_path);
            goto exit;
        }

        free(pool_path);
    }

    if(GET_BOOL(args, "ForceDoraCycle")) {
        force_dora_cycle(ip_intf);
    }

    rv = amxd_status_ok;

exit:
    free(ip_intf);
    free(logical_intf);
    amxc_var_delete(&ip_params);
    amxc_var_delete(&dhcpv4_params);
    amxd_trans_clean(&trans);
    return rv;
}

amxd_status_t _GetIPv4Config(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* dhcp_pool_ref = NULL;
    amxc_var_t* dhcp_params = NULL;
    amxc_var_t pool_ref;
    amxd_trans_t trans;

    amxc_var_init(&pool_ref);
    amxd_trans_init(&trans);

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);
    when_str_empty_trace(ip_intf, exit, ERROR, "Could not find IP interface belonging to %s", logical_intf);

    amxc_var_convert(&pool_ref, amxd_object_get_param_value(object, "DHCPv4PoolReference"), AMXC_VAR_ID_LIST);
    if(str_empty(GETI_CHAR(&pool_ref, 0))) {
        dhcp_pool_ref = get_dhcpv4_pool(ip_intf);
        when_str_empty_trace(dhcp_pool_ref, exit, ERROR, "No DHCPv4 pool found for interface %s", ip_intf);
    } else {
        dhcp_pool_ref = strdup(GETI_CHAR(&pool_ref, 0));
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_private_ipv4_params(skip_prefix(ip_intf), ret), exit, ERROR, "Failed to get private IP parameters");

    dhcp_params = amxc_var_add_new_key(ret, "DHCPParameters");
    amxc_var_set_type(dhcp_params, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_dhcpv4_params(dhcp_pool_ref, dhcp_params), exit, ERROR, "Failed to get DHCPv4 parameters");

    if(str_empty(GETI_CHAR(&pool_ref, 0))) {
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(cstring_t, &trans, "DHCPv4PoolReference", dhcp_pool_ref);
        when_failed_trace(amxd_trans_apply(&trans, logical_get_dm()), exit, ERROR,
                          "Failed to update DHCPv4PoolReference");
    }

    rv = amxd_status_ok;

exit:
    free(logical_intf);
    free(ip_intf);
    free(dhcp_pool_ref);
    amxc_var_clean(&pool_ref);
    amxd_trans_clean(&trans);
    return rv;
}

amxd_status_t _GetIPv4ConfigExt(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    amxc_var_t* dhcp_params = NULL;
    amxc_var_t* ip_params = NULL;
    amxc_var_t* data = NULL;
    amxc_var_t* pools = NULL;
    amxc_string_t str;

    amxc_string_init(&str, 0);

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);
    when_str_empty_trace(ip_intf, exit, ERROR, "Could not find IP interface belonging to %s", logical_intf);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    ip_params = amxc_var_add_key(amxc_llist_t, ret, "IPConfigs", NULL);
    data = amxc_var_add(amxc_htable_t, ip_params, NULL);

    when_failed_trace(get_private_ipv4_params(skip_prefix(ip_intf), data), exit, ERROR, "Failed to get private IP parameters");
    amxc_var_add_key(uint32_t, data, "Index", 1);

    data = amxc_var_add(amxc_htable_t, ip_params, NULL);
    if(get_public_ipv4_params(skip_prefix(ip_intf), data) != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to get public IP parameters");
        amxc_var_delete(&data);
    } else {
        amxc_var_add_key(uint32_t, data, "Index", 2);
    }

    dhcp_params = amxc_var_add_key(amxc_llist_t, ret, "Pools", NULL);

    amxc_string_setf(&str, "DHCPv4Server.Pool.[Interface=='%s'].", ip_intf);
    pools = component_get_path_instances(dhcpv4_get_context(), amxc_string_get(&str, 0));

    amxc_var_for_each(pool, pools) {
        data = amxc_var_add(amxc_htable_t, dhcp_params, NULL);

        if(get_dhcpv4_params(GET_CHAR(pool, NULL), data) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to get DHCP pool parameters for pool %s", GET_CHAR(pool, NULL));
            amxc_var_delete(&data);
        }
    }

    rv = amxd_status_ok;

exit:
    if(rv != amxd_status_ok) {
        amxc_var_clean(ret);
    }

    free(logical_intf);
    free(ip_intf);
    amxc_var_delete(&pools);
    amxc_string_clean(&str);
    return rv;
}

amxd_status_t _SetAlternativeIPv4Config(amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        amxc_var_t* args,
                                        UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    amxc_var_t* ip_params = NULL;

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);
    ip_params = collect_ip_params(args);

    when_str_empty_trace(ip_intf, exit, ERROR, "Failed to get IP interface");

    if(ip_params != NULL) {
        when_failed_trace(set_public_ipv4_params(skip_prefix(ip_intf), ip_params),
                          exit, ERROR, "Failed to set public IPv4 parameters for interface %s", ip_intf);
    }

    rv = amxd_status_ok;

exit:
    free(ip_intf);
    free(logical_intf);
    amxc_var_delete(&ip_params);
    return rv;
}

amxd_status_t _GetAlternativeIPv4Config(amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        UNUSED amxc_var_t* args,
                                        amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);

    when_str_empty_trace(ip_intf, exit, ERROR, "Could not find IP interface belonging to %s", logical_intf);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_public_ipv4_params(skip_prefix(ip_intf), ret), exit, ERROR, "Failed to get public IP parameters");

    rv = amxd_status_ok;

exit:
    free(logical_intf);
    free(ip_intf);
    return rv;
}

amxd_status_t _SetIPv6Config(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    amxc_var_t* dhcpv6_params = NULL;
    amxc_var_t* ra_params = NULL;

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);
    when_str_empty_trace(ip_intf, exit, ERROR, "Failed to get IP interface");
    dhcpv6_params = collect_dhcpv6_params(args, logical_intf);
    ra_params = collect_ra_params(args, ip_intf);

    if(amxc_var_get_key(args, "Enable", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        when_failed_trace(component_set_bool(skip_prefix(ip_intf), ip_get_context(), "IPv6Enable", GET_BOOL(args, "Enable")),
                          exit, ERROR, "Failed to toggle %sIPv6Enable", ip_intf);
    }

    if(dhcpv6_params != NULL) {
        const char* new_pool_alias = GET_CHAR(args, "DHCPv6Pool");
        char* curr_dhcpv6_pool = amxd_object_get_value(cstring_t, object, "DHCPv6PoolReference", NULL);
        char* new_dhcpv6_pool = NULL;

        if(str_empty(curr_dhcpv6_pool)) {
            free(curr_dhcpv6_pool);

            curr_dhcpv6_pool = get_dhcpv6_pool(logical_intf);
        }
        new_dhcpv6_pool = set_dhcpv6_params(curr_dhcpv6_pool, new_pool_alias, dhcpv6_params);

        free(curr_dhcpv6_pool);
        if(str_empty(new_dhcpv6_pool)) {
            SAH_TRACEZ_ERROR(ME, "Failed to update DHCPv6 params");
            free(new_dhcpv6_pool);
            goto exit;
        } else {
            amxd_object_set_value(cstring_t, object, "DHCPv6PoolReference", new_dhcpv6_pool);
            free(new_dhcpv6_pool);
        }
    }

    if(ra_params != NULL) {
        const char* new_pool_alias = GET_CHAR(args, "RAPool");
        char* curr_ra_pool = NULL;
        char* new_ra_pool = NULL;

        curr_ra_pool = get_ra_pool(ip_intf);
        new_ra_pool = set_ra_params(curr_ra_pool, new_pool_alias, ra_params);

        free(curr_ra_pool);
        if(str_empty(new_ra_pool)) {
            SAH_TRACEZ_ERROR(ME, "Failed to update RA params");
            free(new_ra_pool);
            goto exit;
        }
        free(new_ra_pool);
    }

    rv = amxd_status_ok;

exit:
    free(ip_intf);
    free(logical_intf);
    amxc_var_delete(&ra_params);
    amxc_var_delete(&dhcpv6_params);
    return rv;
}

amxd_status_t _GetIPv6Config(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* ip_intf = NULL;
    char* dhcp_pool_ref = NULL;
    amxc_var_t* dhcp_params = NULL;
    amxc_var_t* ra_params = NULL;

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    ip_intf = get_ip_intf(logical_intf);
    when_str_empty_trace(ip_intf, exit, ERROR, "Could not find IP interface belonging to %s", logical_intf);

    dhcp_pool_ref = amxd_object_get_value(cstring_t, object, "DHCPv6PoolReference", NULL);
    if(str_empty(dhcp_pool_ref)) {
        free(dhcp_pool_ref);

        dhcp_pool_ref = get_dhcpv6_pool(logical_intf);
        when_str_empty_trace(dhcp_pool_ref, exit, ERROR, "No DHCPv6 pool found for interface %s", logical_intf);
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_ipv6_params(skip_prefix(ip_intf), ret), exit, ERROR, "Failed to get IPv6 parameters");

    dhcp_params = amxc_var_add_new_key(ret, "DHCPParameters");
    when_null_trace(dhcp_params, exit, ERROR, "Failed to created DHCP parameters variant");
    amxc_var_set_type(dhcp_params, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_dhcpv6_params(dhcp_pool_ref, dhcp_params), exit, ERROR, "Failed to get DHCPv6 parameters");

    ra_params = amxc_var_add_new_key(ret, "RAParameters");
    when_null_trace(ra_params, exit, ERROR, "Failed to created RA parameters variant");
    amxc_var_set_type(ra_params, AMXC_VAR_ID_HTABLE);
    when_failed_trace(get_ra_params(ip_intf, ra_params), exit, ERROR, "Failed to get RA parameters");

    amxd_object_set_value(cstring_t, object, "DHCPv6PoolReference", dhcp_pool_ref);

    rv = amxd_status_ok;
exit:
    free(logical_intf);
    free(ip_intf);
    free(dhcp_pool_ref);
    return rv;
}

amxd_status_t _AddLowerLayerInterface(amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* lower_layer = NULL;
    char* lower_layer_path = NULL;
    char* bridge_port = NULL;
    char* bridge_intf = NULL;
    amxc_string_t bridge_port_search_path;

    amxc_string_init(&bridge_port_search_path, 0);

    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    bridge_intf = get_bridge_intf(logical_intf);
    when_str_empty_trace(bridge_intf, exit, ERROR, "Couldn't find bridge interface");

    lower_layer = component_get_path_instance(device_get_context(), GET_CHAR(args, "LowerLayerInterface"));
    lower_layer_path = trim_final_dot(lower_layer);
    if(str_empty(lower_layer)) {
        SAH_TRACEZ_ERROR(ME, "Lower layer interface %s doesn't exist, can't add it to the bridge interface", GET_CHAR(args, "LowerLayerInterface"));
        rv = amxd_status_object_not_found;
        goto exit;
    }

    amxc_string_setf(&bridge_port_search_path, "Bridging.Bridge.*.Port.[LowerLayers=='%s'].", lower_layer_path);
    bridge_port = component_get_path_instance(bridging_get_context(), amxc_string_get(&bridge_port_search_path, 0));
    if(str_empty(bridge_port)) {
        //LowerLayerInterface doesn't belong to any bridge port, so we can add it
        char* created_bridge_port = create_bridge_port(skip_prefix(bridge_intf), lower_layer_path);

        if(str_empty(created_bridge_port)) {
            SAH_TRACEZ_ERROR(ME, "Failed to created bridge port for LowerLayerInterface %s", lower_layer_path);
        } else {
            SAH_TRACEZ_INFO(ME, "Successfully created new bridge port %s", created_bridge_port);
            rv = amxd_status_ok;
        }
        free(created_bridge_port);
    } else {
        SAH_TRACEZ_ERROR(ME, "LowerLayerInterface %s already belongs to bridge port %s", lower_layer_path, bridge_port);
        rv = amxd_status_invalid_function_argument;
    }

exit:
    free(logical_intf);
    free(lower_layer);
    free(lower_layer_path);
    free(bridge_port);
    free(bridge_intf);
    amxc_string_clean(&bridge_port_search_path);
    return rv;
}

amxd_status_t _RemoveLowerLayerInterface(amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    char* logical_intf = NULL;
    char* lower_layer = NULL;
    char* lower_layer_path = NULL;
    char* bridge_port = NULL;
    char* bridge_port_path = NULL;
    char* ll_bridge_intf = NULL;
    char* bridge_intf = NULL;
    amxc_string_t bridge_port_search_path;

    amxc_string_init(&bridge_port_search_path, 0);
    when_false_trace(is_lan_enabled(amxd_object_get_parent(object)), exit, INFO, "%s is not enabled", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    logical_intf = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    bridge_intf = get_bridge_intf(logical_intf);
    when_str_empty_trace(bridge_intf, exit, ERROR, "Couldn't find bridge interface");

    lower_layer = component_get_path_instance(device_get_context(), GET_CHAR(args, "LowerLayerInterface"));
    lower_layer_path = trim_final_dot(lower_layer);
    if(str_empty(lower_layer)) {
        SAH_TRACEZ_ERROR(ME, "Lower layer interface %s doesn't exist, can't remove it from the bridge interface", GET_CHAR(args, "LowerLayerInterface"));
        rv = amxd_status_object_not_found;
        goto exit;
    }

    amxc_string_setf(&bridge_port_search_path, "Bridging.Bridge.*.Port.[LowerLayers=='%s'].", lower_layer_path);
    bridge_port = component_get_path_instance(bridging_get_context(), amxc_string_get(&bridge_port_search_path, 0));
    bridge_port_path = trim_final_dot(bridge_port);
    if(str_empty(bridge_port_path)) {
        SAH_TRACEZ_WARNING(ME, "LowerLayerInterface %s is not part of a bridge port", lower_layer_path);
        rv = amxd_status_invalid_function_argument;
        goto exit;
    }

    amxc_string_setf(&bridge_port_search_path, "Bridging.Bridge.*.Port.['Device.%s' in LowerLayers].", bridge_port_path);
    ll_bridge_intf = component_get_path_instance(bridging_get_context(), amxc_string_get(&bridge_port_search_path, 0));

    if(str_empty(ll_bridge_intf)) {
        // LowerLayerInterface is not part of a bridge interface, so we can't remove it
        rv = amxd_status_invalid_function_argument;
    } else if(strcmp(ll_bridge_intf, skip_prefix(bridge_intf)) == 0) {
        // LowerLayerInterface is part of the bridge interface, remove it
        if(component_del_instance(bridge_port_path, bridging_get_context()) == 0) {
            rv = amxd_status_ok;
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "LowerLayerInterface %s is part of another bridge %s", lower_layer, ll_bridge_intf);
        rv = amxd_status_invalid_function_argument;
    }

exit:
    free(logical_intf);
    free(lower_layer);
    free(lower_layer_path);
    free(bridge_port);
    free(bridge_port_path);
    free(ll_bridge_intf);
    free(bridge_intf);
    amxc_string_clean(&bridge_port_search_path);
    return rv;
}

void _domain_search_list_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(logical_get_dm(), data);

    set_ipv4_dsl(object);
    set_ipv6_dsl(object);
}

void _domain_name_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(logical_get_dm(), data);

    set_domain_name(object);
    dns_set_domain_name(object, GETP_CHAR(data, "parameters.DomainName.to"));
    set_ipv6_dsl(object);
}

amxd_status_t _read_lan_status(amxd_object_t* object,
                               UNUSED amxd_param_t* param,
                               UNUSED amxd_action_t reason,
                               UNUSED const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               UNUSED void* priv) {
    amxd_object_t* iface_obj = amxd_object_get_parent(object);
    bool enable = is_lan_enabled(iface_obj);
    amxc_var_set(cstring_t, retval, enable ? "Enabled" : "Disabled");
    return amxd_status_ok;
}

amxd_status_t _lan_destroy(amxd_object_t* object,
                           UNUSED amxd_param_t* param,
                           amxd_action_t reason,
                           UNUSED const amxc_var_t* const args,
                           UNUSED amxc_var_t* const retval,
                           UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    lan_priv_free(object);
    status = amxd_status_ok;
exit:
    return status;
}
