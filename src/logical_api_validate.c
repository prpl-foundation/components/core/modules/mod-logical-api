/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <amxb/amxb.h>
#include <ipat/ipat.h>

#include "logical_api_validate.h"
#include "logical_api_utils.h"
#include "logical_api.h"
#include "logical_api_getters.h"
#include "logical_api_component.h"

#define ME "validate"

static bool is_address_allowed(const char* address) {
    bool rv = false;
    amxd_object_t* logical_config = NULL;
    const amxc_var_t* whitelist_csv = NULL;
    const amxc_var_t* blacklist_csv = NULL;
    amxc_var_t lvalid;
    amxc_var_t linvalid;

    amxc_var_init(&lvalid);
    amxc_var_init(&linvalid);

    when_str_empty_trace(address, exit, ERROR, "address is empty");
    logical_config = amxd_dm_findf(logical_get_dm(), "Logical.%sSubnet.Config.", logical_get_prefix());
    when_null_trace(logical_config, exit, ERROR, "Failed to find object Logical.%sSubnet.Config.", logical_get_prefix());

    whitelist_csv = amxd_object_get_param_value(logical_config, "ValidSubnets");
    blacklist_csv = amxd_object_get_param_value(logical_config, "InvalidSubnets");
    amxc_var_convert(&lvalid, whitelist_csv, AMXC_VAR_ID_LIST);
    amxc_var_convert(&linvalid, blacklist_csv, AMXC_VAR_ID_LIST);

    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &lvalid))) { // Whitelist entries
        amxc_var_for_each(valid, &lvalid) {
            const char* subnet = GET_CHAR(valid, NULL);
            if(ipat_text_in_subnet(address, subnet)) {
                // Match in whitelist -> allow address
                rv = true;
                goto exit;
            }
        }
        // No match in whitelist -> do not allow address
        rv = false;
    } else if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &linvalid))) { // Blacklist entries
        amxc_var_for_each(invalid, &linvalid) {
            const char* subnet = GET_CHAR(invalid, NULL);
            if(ipat_text_in_subnet(address, subnet)) {
                // Match in blacklist -> do not allow address
                rv = false;
                goto exit;
            }
        }
        // No match in the blacklist -> allow the address
        rv = true;
    } else { // No entries in both whitelist and blacklist -> allow the address
        rv = true;
    }

exit:
    amxc_var_clean(&linvalid);
    amxc_var_clean(&lvalid);
    return rv;
}

static char* ip_addr_to_subnet(const char* ip_addr, const char* subnet_mask) {
    amxc_string_t ip;
    char* subnet = NULL;

    amxc_string_init(&ip, 0);
    when_str_empty(ip_addr, exit);
    when_str_empty(subnet_mask, exit);

    amxc_string_setf(&ip, "%s/%s", ip_addr, subnet_mask);
    subnet = ipat_text_to_subnet(amxc_string_get(&ip, 0));

exit:
    amxc_string_clean(&ip);
    return subnet;
}

static bool conflicting_subnet_exists(const char* addr, const char* ip_intf) {
    amxc_string_t private_address_search_path;
    bool rv = false;
    amxc_var_t result;
    const char* alias = NULL;

    amxc_string_init(&private_address_search_path, 0);
    amxc_var_init(&result);

    when_str_empty_trace(addr, exit, ERROR, "IP address in empty");
    when_str_empty_trace(ip_intf, exit, ERROR, "IP interface is empty");

    component_get_param(&result, skip_prefix(ip_intf), ip_get_context(), "Alias");
    alias = GETP_CHAR(&result, "0.0.Alias");

    when_str_empty_trace(alias, exit, ERROR, "Couldn't find alias for IP interface %s", ip_intf);

    amxc_string_setf(&private_address_search_path,
                     "IP.Interface.*.IPv4Address.[TypeFlags == '@private' && AddressingType == 'Static' && Alias != '%s'].", alias);

    amxc_var_clean(&result);
    amxc_var_init(&result);
    when_failed_trace(multiple_components_get_parameters(&result, amxc_string_get(&private_address_search_path, 0), ip_get_context()),
                      exit, ERROR, "Failed to get all private LAN IP addresses");

    amxc_var_for_each(var, GETI_ARG(&result, 0)) {
        const char* ip_addr = GET_CHAR(var, "IPAddress");
        const char* subnet_mask = GET_CHAR(var, "SubnetMask");
        char* subnet = ip_addr_to_subnet(ip_addr, subnet_mask);

        if(subnet == NULL) {
            continue;
        } else {
            if(ipat_text_in_subnet(addr, subnet)) {
                free(subnet);
                goto exit;
            }
        }
        free(subnet);
    }
    rv = true;
exit:
    amxc_var_clean(&result);
    amxc_string_clean(&private_address_search_path);
    return rv;
}

bool is_valid_ip_network(amxc_var_t* ip_params, const char* ip_intf) {
    const char* ip_addr = GET_CHAR(ip_params, "IPAddress");
    const char* subnet_mask = GET_CHAR(ip_params, "SubnetMask");
    bool rv = false;
    amxc_string_t address;
    amxc_var_t missing_params;

    amxc_var_init(&missing_params);
    amxc_string_init(&address, 0);
    amxc_var_set_type(&missing_params, AMXC_VAR_ID_HTABLE);
    when_str_empty_trace(ip_intf, exit, ERROR, "IP interface is empty");

    if((ip_addr == NULL) && (subnet_mask == NULL)) {
        goto exit;
    } else if(ip_addr == NULL) {
        when_failed_trace(get_private_ipv4_params(ip_intf, &missing_params),
                          exit, ERROR, "Failed to collect missing IP parameters for validation");
        ip_addr = GET_CHAR(&missing_params, "IPAddress");
    } else if(subnet_mask == NULL) {
        when_failed_trace(get_private_ipv4_params(ip_intf, &missing_params),
                          exit, ERROR, "Failed to collect missing IP parameters for validation");
        subnet_mask = GET_CHAR(&missing_params, "SubnetMask");
    }

    amxc_string_setf(&address, "%s/%s", ip_addr, subnet_mask);

    rv = is_address_allowed(ip_addr);
    if(!rv) {
        goto exit;
    }
    rv = conflicting_subnet_exists(amxc_string_get(&address, 0), ip_intf);

exit:
    amxc_var_clean(&missing_params);
    amxc_string_clean(&address);
    return rv;

}
